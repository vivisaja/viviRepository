@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Profil</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Profil</li>
          </ol>
@stop
@section('content')
          
    <!-- right column -->
<div>
    <!-- Horizontal Form -->
    <div class="box box-info">
    <!-- /.box-header -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- form start -->
    <form id="" class="form-horizontal" role="form" method="POST" action="">
        @csrf
        @method("PUT")
        <div class="box-body">
             <div class="form-group row">
                <label for="" class="col-sm-2 control-label">Password<span class="required">*</span></label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
            <label for="" class="col-sm-2 control-label">Ketik Ulang Password<span class="required">*</span></label>
                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
        
        <!-- /.box-body -->
        <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
            <div class="form-actions">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
                 <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
            </div>
            </div>
        <!-- /.box-footer -->
    </form>
    </div>
    <!-- /.box -->
        

@endsection

