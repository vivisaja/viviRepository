<?php

use Illuminate\Database\Seeder;

use App\Repositories\RUsergroup;

class UsergroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Admin',
                'information' => 'Admin'
            ],
            [
                'id' => 2,
                'name' => 'Dosen',
                'information' => 'Dosen'
            ],
            [
                'id' => 3,
                'name' => 'Mahasiswa',
                'information' => 'Mahasiswa'
            ]
            ];
            RUsergroup::runSeeder($data);
    }
}
