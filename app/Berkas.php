<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
    protected $table = "berkas";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'admin_id','nama','url_file','status'
    ];
}
