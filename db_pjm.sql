-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2018 at 11:47 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pjm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_id`, `jabatan`) VALUES
(1, 1, 'Ketua');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `id` int(10) UNSIGNED NOT NULL,
  `mahasiswa_id` int(10) UNSIGNED NOT NULL,
  `tahun_lulus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidang_pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_bekerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lama_bekerja` int(11) NOT NULL,
  `masa_tunggu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id`, `mahasiswa_id`, `tahun_lulus`, `bidang_pekerjaan`, `pekerjaan`, `tempat_bekerja`, `email_instansi`, `lama_bekerja`, `masa_tunggu`) VALUES
(2, 2, 'null', 'null', 'null', 'null', 'null', 0, 0),
(3, 3, 'null', 'null', 'null', 'null', 'null', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `berkas`
--

CREATE TABLE `berkas` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `berkas`
--

INSERT INTO `berkas` (`id`, `admin_id`, `nama`, `url_file`, `status`) VALUES
(1, 1, 'cover', 'public/files/Kbjye5KCluEbBRijebXMS8reVvADqW0YjdxjPAtO.pdf', '0'),
(2, 1, 'SOP skripsi', 'public/files/mTvW1f8C3qJGgHs6M6zTSV7QAhxkJO7HTkIL5weV.docx', '1');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nidn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id`, `user_id`, `nidn`) VALUES
(1, 6, '199610182019042001'),
(2, 7, '199610182019042002');

-- --------------------------------------------------------

--
-- Table structure for table `jawabkuisioner`
--

CREATE TABLE `jawabkuisioner` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `dosen_id` int(10) UNSIGNED NOT NULL,
  `matkul_id` int(10) UNSIGNED NOT NULL,
  `pertanyaan_id` int(10) UNSIGNED NOT NULL,
  `tahun_ajar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawaban` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi`
--

CREATE TABLE `kompetensi` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `kompetensi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kompetensi`
--

INSERT INTO `kompetensi` (`id`, `admin_id`, `kompetensi`) VALUES
(1, 1, 'Pedagogik'),
(2, 1, 'Profesional'),
(3, 1, 'Kepribadian'),
(4, 1, 'Sosial');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `angkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_alumni` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `user_id`, `nim`, `angkatan`, `is_alumni`) VALUES
(2, 3, '140631100080', '2014', '0'),
(3, 5, '140631100089', '2014', '1');

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`id`, `admin_id`, `nama`, `sks`) VALUES
(1, 1, 'Pemograman web', 3),
(2, 1, 'Sosiologi Pendidikan', 3),
(3, 1, 'Teori belajar dan pembelajaran', 3);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_04_26_000001_create_usergroups_table', 1),
(3, '2018_04_26_000002_create_users_table', 1),
(4, '2018_04_26_000003_create_admin_table', 1),
(5, '2018_04_26_000004_create_dosen_table', 1),
(6, '2018_04_26_000005_create_mahasiswa_table', 1),
(7, '2018_04_26_000007_create_berkas_table', 1),
(8, '2018_04_26_000008_create_matkul_table', 1),
(9, '2018_04_26_000009_create_kompetensi_table', 1),
(10, '2018_04_26_000010_create_pertanyaan_table', 1),
(11, '2018_04_26_000011_create_jawabkuisioner_table', 1),
(12, '2018_04_26_000012_create_tracer_table', 1),
(13, '2018_04_26_000013_create_alumni_table', 1),
(14, '2018_04_26_100002_alter_users_table', 1),
(15, '2018_04_26_100003_alter_admin_table', 1),
(16, '2018_04_26_100004_alter_dosen_table', 1),
(17, '2018_04_26_100005_alter_mahasiswa_table', 1),
(18, '2018_04_26_100007_alter_berkas_table', 1),
(19, '2018_04_26_100008_alter_matkul_table', 1),
(20, '2018_04_26_100009_alter_kompetensi_table', 1),
(21, '2018_04_26_100010_alter_pertanyaan_table', 1),
(22, '2018_04_26_100011_alter_jawabkuisioner_table', 1),
(23, '2018_04_26_100012_alter_tracer_table', 1),
(24, '2018_04_26_100013_alter_alumni_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `kompetensi_id` int(10) UNSIGNED NOT NULL,
  `pertanyaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `admin_id`, `kompetensi_id`, `pertanyaan`) VALUES
(1, 1, 1, 'Kelengkapan atribut mata kuliah (meliputi:kontrak kuliah, SP, RPS, & media ajar)'),
(2, 1, 1, 'Kesiapan memberikan kuliah dan atau praktik'),
(3, 1, 2, 'profesional 1'),
(4, 1, 2, 'profesional 2'),
(5, 1, 3, 'Kepribadian 1'),
(6, 1, 3, 'Kepribadian 2'),
(7, 1, 4, 'Sosial 1'),
(8, 1, 4, 'Sosial 2');

-- --------------------------------------------------------

--
-- Table structure for table `tracer`
--

CREATE TABLE `tracer` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `nm_instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_responden` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_res` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_karyawan` int(11) NOT NULL,
  `jml_karyawan_alumni` int(11) NOT NULL,
  `rata_masa_kerja_alumni` int(11) NOT NULL,
  `penghasilan_alumni` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan_alumni` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kerjasama` int(11) NOT NULL,
  `disiplin` int(11) NOT NULL,
  `etika` int(11) NOT NULL,
  `keuletan` int(11) NOT NULL,
  `kemampuan_teori` int(11) NOT NULL,
  `kemampuan_praktik` int(11) NOT NULL,
  `rasa_pd` int(11) NOT NULL,
  `ketelitian` int(11) NOT NULL,
  `kreatifitas` int(11) NOT NULL,
  `kepemimpinan` int(11) NOT NULL,
  `tanggung_jawab` int(11) NOT NULL,
  `keunggulan_alumni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelemahan_alumni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kualitas_alumni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kemampuan_alumni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran_alumni` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran_pif` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tracer`
--

INSERT INTO `tracer` (`id`, `admin_id`, `nm_instansi`, `nm_responden`, `jabatan_res`, `alamat`, `jml_karyawan`, `jml_karyawan_alumni`, `rata_masa_kerja_alumni`, `penghasilan_alumni`, `jabatan_alumni`, `kerjasama`, `disiplin`, `etika`, `keuletan`, `kemampuan_teori`, `kemampuan_praktik`, `rasa_pd`, `ketelitian`, `kreatifitas`, `kepemimpinan`, `tanggung_jawab`, `keunggulan_alumni`, `kelemahan_alumni`, `kualitas_alumni`, `kemampuan_alumni`, `saran_alumni`, `saran_pif`, `created_at`, `updated_at`) VALUES
(1, 1, 'PT Makmur Abadi', 'Abdul kholik', 'Direktur', 'Malang', 200, 20, 2, '7', 'manajer', 4, 3, 3, 3, 4, 3, 2, 3, 4, 4, 4, 'pintar cerdik', 'malas', 'baguss', 'kuat', 'lebih ditingkatkan ilmunya', 'lebih aktif lagi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`id`, `name`, `information`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', '2018-05-23 07:51:15', '2018-05-23 07:51:15'),
(2, 'Dosen', 'Dosen', '2018-05-23 07:51:15', '2018-05-23 07:51:15'),
(3, 'Mahasiswa', 'Mahasiswa', '2018-05-23 07:51:15', '2018-05-23 07:51:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usergroup_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `usergroup_id`, `nama`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@gmail.com', 'admin', '$2y$10$QwruY9hTWFkihs8jY8lMB.LtaIlZWiCfkDWIQJHLpBwecXJQVhUqO', 'aqvCOZzovvtQ2Hf9MTGPjhabMt7xJgW5VYQLuIdWriHw6VHdEpSCPo8BL1cS', '2018-05-23 07:51:16', '2018-05-23 07:51:16'),
(3, 3, 'v1', 'vikhonick@gmail.com', '140631100080', '$2y$10$KrKylBwmnhkiBI8LVXrS7u7Vg/Pg6QGPSoSZRouXZj8TrNuHZigme', NULL, '2018-05-23 08:03:49', '2018-05-23 08:03:49'),
(5, 3, 'Vivi Oktavia', 'vi@yahoo.com', '140631100089', '$2y$10$ukO7cwf8D3y9D70SXP8CCu1/Vixz8mkJne.iJsG8CU710mjPFmv9G', NULL, '2018-05-23 11:28:54', '2018-05-23 11:28:54'),
(6, 2, 'Vivi Oktavia', 'vivisaja@gmail.com', '199610182019042001', '$2y$10$TgZ7z7qST89xsIOmSwm9Mesud7d8/38XP5.OPGqjH07LNpa6ZZ.eq', NULL, '2018-05-23 14:40:57', '2018-05-23 14:40:57'),
(7, 2, 'Nabila', 'nabila@gmail.com', '199610182019042002', '$2y$10$pe5y/ql7kNYR2nFbI2ybJuFjsMjvbswroqeg6c.y0L2ZfOmAeZ0Di', NULL, '2018-05-23 14:42:23', '2018-05-23 14:42:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_user_id_index` (`user_id`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alumni_mahasiswa_id_index` (`mahasiswa_id`);

--
-- Indexes for table `berkas`
--
ALTER TABLE `berkas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `berkas_admin_id_index` (`admin_id`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dosen_nidn_unique` (`nidn`),
  ADD KEY `dosen_user_id_index` (`user_id`);

--
-- Indexes for table `jawabkuisioner`
--
ALTER TABLE `jawabkuisioner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jawabkuisioner_user_id_index` (`user_id`),
  ADD KEY `jawabkuisioner_dosen_id_index` (`dosen_id`),
  ADD KEY `jawabkuisioner_matkul_id_index` (`matkul_id`),
  ADD KEY `jawabkuisioner_pertanyaan_id_index` (`pertanyaan_id`);

--
-- Indexes for table `kompetensi`
--
ALTER TABLE `kompetensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kompetensi_admin_id_index` (`admin_id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mahasiswa_nim_unique` (`nim`),
  ADD KEY `mahasiswa_user_id_index` (`user_id`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matkul_admin_id_index` (`admin_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pertanyaan_admin_id_index` (`admin_id`),
  ADD KEY `pertanyaan_kompetensi_id_index` (`kompetensi_id`);

--
-- Indexes for table `tracer`
--
ALTER TABLE `tracer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tracer_admin_id_index` (`admin_id`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_usergroup_id_index` (`usergroup_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `berkas`
--
ALTER TABLE `berkas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jawabkuisioner`
--
ALTER TABLE `jawabkuisioner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kompetensi`
--
ALTER TABLE `kompetensi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `matkul`
--
ALTER TABLE `matkul`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tracer`
--
ALTER TABLE `tracer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `alumni`
--
ALTER TABLE `alumni`
  ADD CONSTRAINT `alumni_mahasiswa_id_foreign` FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`id`);

--
-- Constraints for table `berkas`
--
ALTER TABLE `berkas`
  ADD CONSTRAINT `berkas_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `dosen_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `jawabkuisioner`
--
ALTER TABLE `jawabkuisioner`
  ADD CONSTRAINT `jawabkuisioner_dosen_id_foreign` FOREIGN KEY (`dosen_id`) REFERENCES `dosen` (`id`),
  ADD CONSTRAINT `jawabkuisioner_matkul_id_foreign` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id`),
  ADD CONSTRAINT `jawabkuisioner_pertanyaan_id_foreign` FOREIGN KEY (`pertanyaan_id`) REFERENCES `pertanyaan` (`id`),
  ADD CONSTRAINT `jawabkuisioner_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `kompetensi`
--
ALTER TABLE `kompetensi`
  ADD CONSTRAINT `kompetensi_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `matkul`
--
ALTER TABLE `matkul`
  ADD CONSTRAINT `matkul_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD CONSTRAINT `pertanyaan_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `pertanyaan_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`);

--
-- Constraints for table `tracer`
--
ALTER TABLE `tracer`
  ADD CONSTRAINT `tracer_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_usergroup_id_foreign` FOREIGN KEY (`usergroup_id`) REFERENCES `usergroups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
