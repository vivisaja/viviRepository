<?php
namespace App\Helpers;

class User
{
    const USERGROUP_ID_ADMIN = 1;
    const USERGROUP_ID_DOSEN = 2;
	const USERGROUP_ID_MAHASISWA = 3;
	const mahasiswa_is_alumni = 1;

	public static function isAdmin()
	{
		if (\Auth::user()->usergroup_id == self::USERGROUP_ID_ADMIN) {
			return true;
		}

		return false;
	}

	public static function isDosen()
	{
		if (\Auth::user()->usergroup_id == self::USERGROUP_ID_DOSEN) {
			return true;
		}

		return false;
    }
    public static function isMahasiswa()
	{
		if (\Auth::user()->usergroup_id == self::USERGROUP_ID_MAHASISWA) {
			return true;
		}

		return false;
	}

	public static function isAlumni()
	{
		if (\Auth::user()->usergroup_id == self::USERGROUP_ID_MAHASISWA && 
		\Auth::user()->mahasiswa->is_alumni == self::mahasiswa_is_alumni) {
			return true;
		}

		return false;
	}
}