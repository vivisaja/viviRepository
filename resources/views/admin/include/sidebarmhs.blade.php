<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{route ('home')}}">
            <i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
        <li><a href="{{route('kuisioner')}}"><i class="fa fa-pencil"></i> <span>Isi Kuisioner</span></a></li>
        <li><a href="{{route('berkas.index')}}"><i class="fa fa-link"></i> <span>Unduh Berkas PJM</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>