<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matkul extends Model
{
    protected $table = "matkul";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'admin_id','nama','sks'
    ];

    public function JawabKuisioner(){
        return $this->hasOne('App\Jawabkuisioner');
    }
}
