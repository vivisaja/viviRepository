<?php 
namespace App\Repositories;
use App\Admin;

class RAdmin{
    public static function runSeeder ($data){
        for ($i=0; $i < count($data) ; $i++) {
            $item = $data[$i];
            $admin = new Admin();
            $admin->user_id = $item['user_id'];
            $admin->jabatan = $item['jabatan'];
            $admin->save();
        }
    }
}