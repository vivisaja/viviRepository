@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Isi Kuisioner</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Isi Kuisioner</li>
          </ol>
@stop
@section('content')
          
<!-- right column -->
<div>
  <div class="box box-info">
  {{--  <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storePertanyaan') }}">  --}}
    <form id="" class="form-horizontal" role="form" method="POST" action="{{route('storeKuisioner')}}">
      @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Tahun Ajaran<span class="required">*</span></label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="" name="tahun_ajar" placeholder="Contoh : 2018">
            </div>
        </div>

        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Semester<span class="required">*</span></label>
            <div class="col-sm-5">
              <input type="text" class="form-control" id="" name="semester" placeholder="Contoh : Genap">
            </div>
        </div>

      <!--harusnya ambil daari tabel dosen-->
      <div class="form-group">
        <label for="" class="col-sm-2 control-label">Nama Dosen<span class="required">*</span></label>
          <div class="col-sm-7">
            <select class="form-control select2 select2-hidden-accessible" name="dosen_id" style="width: 50%;" tabindex="-1" aria-hidden="true">
              <option value="">Silahkan Pilih</option>
                @foreach ($users as $users)
                  <option value="{{ $users->id }}">
                      {{$users->nama}}</option>
                @endforeach
              </select>
              @if ($errors->has('id'))
                <span class="help-block">
                  <font style="color:crimson"> {{ $errors->first('id') }}</font>                                
                </span>
              @endif
          </div>
        </div>

        {{--  {{--  harusnya ambil data dari tabel matkul  --}}
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Nama Mata Kuliah<span class="required">*</span></label>
            <div class="col-sm-7">
              <select class="form-control select2 select2-hidden-accessible" name="matkul_id" style="width: 50%;" tabindex="-1" aria-hidden="true">
              <option value="">Silahkan Pilih</option>
              @foreach ($matkul as $matkul)
                <option value="{{ $matkul->id }}">
                    {{$matkul->nama}}</option>
                @endforeach
              </select>
              @if ($errors->has('matkul_id'))
                <span class="help-block">
                  <font style="color:crimson"> {{ $errors->first('matkul_id') }}</font>                                
                </span>
              @endif
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
          <table>
            <thead><tr><th colspan="2"><strong>Petunjuk :</strong></th></tr></thead>
            <tbody>
              <tr>
                <td colspan="2"> Penilaian dilakukan terhadap aspek-aspek dalam tabel berikut dengan kriteria rentang skor 1 sampai dengan 4</td>
              </tr>
            <tr>
              <td style="width:50%"> 1 = tidak baik/rendah/kurang lengkap </td>
              <td style="width:50%"> 2 = biasa/cukup/kadang-kadang/cukup lengkap </td>
            </tr>
            <tr>
              <td> 3 = baik/tinggi/sering/lengkap </td>
              <td> 4 = sangat baik/sangat tinggi/selalu/sangat lengkap </td>
            </tr>
          </tbody>
          </table>
          <br>
        </div>

        {{--  tampilkan pertanyaan  --}}
        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-0">
          @php $ID = 1; @endphp
          @php $n = 1; @endphp
          @foreach($kompetensi as $kompetensi)
            <strong> {{$ID++}}. Kompetensi {{$kompetensi->kompetensi}} </strong>
            <table class="table table-bordered table-hover" style="width:950%">
              <thead>
                <tr>
                <th style="width:5%; text-align: center; vertical-align:middle" rowspan="2">No</th>
                <th style="width:50%; text-align: center; vertical-align:middle" rowspan="2">Butir-butir yang dinilai</th>
                <th style="width:45%; text-align: center" colspan="4">Skor</th>
                </tr>
                <tr>
                  <th style="text-align: center">1</th>
                  <th style="text-align: center">2</th>
                  <th style="text-align: center">3</th>
                  <th style="text-align: center">4</th>
                </tr>
              </thead>
              @php $no = 1; @endphp
              <tbody>
                @if(count($kompetensi->pertanyaan))
                  @foreach($kompetensi->pertanyaan as $pertanyaan)
                <tr>
                <td style="width:5%; text-align: center">{{$no++}}</td>
                <td style="width:50%">
                    {{$pertanyaan->pertanyaan}} <br>
                    </td>
                <td style="text-align: center"><input type="radio" id="rendah"  name="{{$pertanyaan->id}}[jawaban]" value="1" { { old('{{$pertanyaan->id}}.[jawaban]')=="1" ? 'checked='.'"'.'checked'.'"' : '' } } required="true"></td>
                <td style="text-align: center"><input type="radio" id="cukup" name="{{$pertanyaan->id}}[jawaban]" value="2" { { old('{{$pertanyaan->id}}.[jawaban]')=="1" ? 'checked='.'"'.'checked'.'"' : '' } } required="true"></td>
                <td style="text-align: center"><input type="radio" id="baik" name="{{$pertanyaan->id}}[jawaban]" value="3" { { old('{{$pertanyaan->id}}.[jawaban]')=="1" ? 'checked='.'"'.'checked'.'"' : '' } } required="true"></td>
                <td style="text-align: center"><input type="radio" id="tinggi" name="{{$pertanyaan->id}}[jawaban]" value="4" { { old('{{$pertanyaan->id}}.[jawaban]')=="1" ? 'checked='.'"'.'checked'.'"' : '' } } required="true" ></td>
                </tr>
                  @endforeach
                  @endif
              <tr>
                <td></td>
                <td style="text-align:right"><strong> Jumlah Skor {{$n++}}</strong></td>
                <td colspan="4"></td>
              </tr>
              </tbody>
            </table>
            @endforeach
        </div>
      </div>
        

<!-- /.box-body -->
<div class="box-footer" style="width: 59%;">
    <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
          <a href="{{ route('pertanyaan') }}" class="btn btn-primary" type="button">Kembali</a>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
</div>
        

@endsection

