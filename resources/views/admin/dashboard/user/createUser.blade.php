@extends('layouts.main')

@section('isinya')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3></h3>
      </div>

    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{ $judul }}</h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form method="{{ $method }}" action="{{ $action }}" id="demo-form2" data-parsley-validate class="form_ajax form-horizontal form-label-left">
                {{ csrf_field() }}

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Level <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="form-control" name="level_id">
                    <option value=""> Silahkan Pilih</option>
                    @foreach ($levels as $r)
                      <option value="{{ $r->id }}" {{ $r->id == @$item->level_id ? 'selected' : '' }}>{{ $r->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


               <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jurusan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   <select class="form-control" name="jurusan_id">
                    <option value=""> Silahkan Pilih</option>
                    @foreach ($jurusans as $r)
                      <option value="{{ $r->id }}" {{ $r->id == @$item->jurusan_id ? 'selected' : '' }}>{{ $r->kode }} - {{ $r->nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="name" value="{{ @$item->name }}" id="first-name" class="form-control col-md-7 col-xs-12" placeholder="nama pengguna">
                </div>
              </div>

             <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">jenis kelamin <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   <select class="form-control" name="jk">
                    <option value=""> Silahkan Pilih</option>
                    <option value="l" {{ @$item->jk=='l' ? 'selected' : '' }}> Laki-laki</option>
                    <option value="p" {{ @$item->jk=='p' ? 'selected' : '' }}> Perempuan</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="email" value="{{ @$item->email }}" class="form-control col-md-7 col-xs-12" placeholder="email">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" name="password" value="" class="form-control col-md-7 col-xs-12" placeholder="Password">
                  @if ($mode=='edit')
                    <span>Kosongkan jika tidak ingin diganti</span>
                  @endif
                </div>
              </div>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="{{ route('user.index') }}" class="btn btn-primary" type="button">Kembali</a>
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
@endsection

@section('js')
<script src="{{ asset('js/form_general.js') }}"></script>
<script type="text/javascript">

</script>
@endsection
