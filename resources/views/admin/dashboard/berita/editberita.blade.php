@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Berita</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Berita</li>
          </ol>
@stop
@section('content')
          
<!-- Horizontal Form -->
<div class="box box-info">
  <!-- form start -->
  <form id="" class="form-horizontal" role="form" method="put" action="{{ route('updateBerita') }}">
    <input type ="hidden" name="_token" value="{{ csrf_token() }}">
    @method ("PUT")
    <div class="box-body">
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Judul<span class="required">*</span></label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="judul" placeholder="Ketikkan Judul Berita" value="{{$berita->judul ??}}">
        </div>
      </div>
      <div class="form-group">
        <label for="" class="col-sm-2 control-label">Isi Berita<span class="required">*</span></label>
        <div class="col-sm-10">
          <textarea name ="isi_berita" cols="30" rows="10" class="form-control" id="" placeholder="Ketikkan isi berita"></textarea>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="width: 59%;">
        <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
              <a href="{{ route('berita') }}" class="btn btn-primary" type="button">Kembali</a>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </div>
      </div>
      
    <!-- /.box-footer -->
  </form>
</div>
<!-- /.box -->

@endsection

