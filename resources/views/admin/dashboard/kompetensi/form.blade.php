@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Data Kompetensi</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Data Kompetensi</li>
          </ol>
@stop
@section('content')
<div>
<div class="box box-info">
  <!-- /.box-header -->
  
  <!-- form start -->
  <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storeKompetensi') }}">
    @csrf
    <div class="box-body">
      <div class="form-group">
        <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
        <div class="col-sm-10">
          <input type="text" class="form-control{{ $errors->has('kompetensi') ? ' is-invalid' : '' }}" id="" name="kompetensi" placeholder="Nama Kompetensi">
          @if ($errors->has('kompetensi'))
            <span class="invalid-feedback">
              <font style="color:crimson"> {{ $errors->first('kompetensi') }}</font>
            </span>
          @endif
        </div>
      </div>
    <!-- /.box-body -->
    <div class="box-footer" style="width: 59%;">
        <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
              <a href="{{ route('kompetensi') }}" class="btn btn-primary" type="button">Kembali</a>
              <button type="submit" class="btn btn-success">Simpan</button>
            </div>
          </div>
        </div>
      </div>
    <!-- /.box-footer -->
  </form>
</div>
<!-- /.box -->
        

@endsection

