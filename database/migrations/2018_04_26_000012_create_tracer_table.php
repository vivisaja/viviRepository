<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned();
            $table->string('nm_instansi');
            $table->string('nm_responden');
            $table->string('jabatan_res');
            $table->string('alamat');
            $table->integer('jml_karyawan');
            $table->integer('jml_karyawan_alumni');
            $table->integer('rata_masa_kerja_alumni');
            $table->string('penghasilan_alumni');
            $table->string('jabatan_alumni');
            $table->integer('kerjasama');
            $table->integer('disiplin');
            $table->integer('etika');
            $table->integer('keuletan');
            $table->integer('kemampuan_teori');
            $table->integer('kemampuan_praktik');
            $table->integer('rasa_pd');
            $table->integer('ketelitian');
            $table->integer('kreatifitas');
            $table->integer('kepemimpinan');
            $table->integer('tanggung_jawab');
            $table->text('keunggulan_alumni');
            $table->text('kelemahan_alumni');
            $table->text('kualitas_alumni');
            $table->text('kemampuan_alumni');
            $table->text('saran_alumni');
            $table->text('saran_pif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracer');
    }
}
