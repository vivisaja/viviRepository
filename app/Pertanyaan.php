<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'admin_id','kompetensi_id','pertanyaan'
    ];

    public function Kompetensi(){
        return $this->belongsTo('App\Kompetensi');
    }

    public function Jawabkuisioner(){
        return $this->hasOne('App\Jawabkuisioner');
    }
    
}
