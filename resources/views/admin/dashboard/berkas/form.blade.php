@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Berkas</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Berkas</li>
          </ol>
@stop
@section('content')
          
          <!-- right column -->
        <div>
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- /.box-header -->
            <!-- form start -->
            <form id="" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('storeBerkas') }}">
              @csrf
              <div class="box-body">
                  <div class="form-group">
                      <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" id="nama" placeholder="Ketikkan Nama Berkas" value="{{ old('nama') }}">
                        @if ($errors->has('nama'))
                          <span class="invalid-feedback">
                            <font style="color:crimson"> {{ $errors->first('nama') }}</font>                                
                          </span>
                        @endif
                      </div>
                    </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">File input<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="file" id="url_file" name="url_file">
                    <p class="help-block"></p>
                  </div>
                </div>

                <div class="form-group ">
                    <label for="" class="col-sm-2 control-label">Status Berkas<span class="required">*</span></label>
                      <div class="col-sm-10 has-error">
                        <select class="form-control" name="status">
                            <option value=""> Silahkan Pilih</option>
                            <option value="0">Umum</option>
                            <option value="1">Khusus</option>
                        </select>
                        @if ($errors->has('status'))
                          <span class="help-block">
                            <font style="color:crimson"> {{ $errors->first('status') }}</font>                                
                          </span>
                        @endif
                      </div>
                  </div>
              <!-- /.box-body -->
              <div class="box-footer" style="width: 59%;">
                  <div class="ln_solid"></div>
                    <div class="form-actions">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                        <a href="{{ route('berkas') }}" class="btn btn-primary" type="button">Kembali</a>
                        <button type="submit" class="btn btn-success">Simpan</button>
                      </div>
                    </div>
                  </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        

@endsection

