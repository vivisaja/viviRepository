<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/changePassword','HomeController@showChangePasswordForm')->name('showform');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');


// usergroup admin
Route::group(['middleware'=>['web','auth','usergroup_id:1']],
function()
{
   
    //route dosen
    Route::get('/dosen', 'DosenController@index')->name('dosen');
    // Route::get('/dosen/:id', 'DosenController@show')->name('detailDosen');
    Route::get('/dosen/create', 'DosenController@create')->name('addDosen');
    Route::post('/dosen', 'DosenController@store')->name('storeDosen');
    Route::get('/dosen/{id}/edit', 'DosenController@edit')->name('editDosen');
    Route::put('/dosen/{id}', 'DosenController@update')->name('updateDosen');
    Route::delete('/dosen/{id}', 'DosenController@destroy')->name('destroyDosen');

    //route mahasiswa
    Route::get('/mahasiswa', 'MahasiswaController@index')->name('mahasiswa');
    Route::post('/mahasiswa/ajax-list', 'MahasiswaController@ajaxList')->name('mahasiswa.ajaxList'); // untuk table
    // Route::get('/mahasiswa/:id', 'MahasiswaController@show')->name('detailMahasiswa');
    Route::get('/mahasiswa/create', 'MahasiswaController@create')->name('addMahasiswa');
    Route::post('/mahasiswa', 'MahasiswaController@store')->name('storeMahasiswa');
    Route::get('/mahasiswa/{id}/edit', 'MahasiswaController@edit')->name('editMahasiswa');
    Route::put('/mahasiswa/{id}', 'MahasiswaController@update')->name('updateMahasiswa');
    Route::delete('/mahasiswa/{id}', 'MahasiswaController@destroy')->name('destroyMahasiswa');

    //route matkul
    Route::get('/matkul', 'MatkulController@index')->name('matkul');
    // Route::get('/matkul/:id', 'MatkulController@show')->name('detailMatkul');
    Route::get('/matkul/create', 'MatkulController@create')->name('addMatkul');
    Route::post('/matkul', 'MatkulController@store')->name('storeMatkul');
    Route::get('/matkul/{id}/edit', 'MatkulController@edit')->name('editMatkul');
    Route::put('/matkul/{id}', 'MatkulController@update')->name('updateMatkul');
    Route::delete('/matkul/{id}', 'MatkulController@destroy')->name('destroyMatkul');

    //route Kompetensi
    Route::get('/kompetensi', 'KompetensiController@index')->name('kompetensi');
    // Route::get('/kompetensi/:id', 'KompetensiController@show')->name('detailKompetensi');
    Route::get('/kompetensi/create', 'KompetensiController@create')->name('addKompetensi');
    Route::post('/kompetensi', 'KompetensiController@store')->name('storeKompetensi');
    Route::get('/kompetensi/{id}/edit', 'KompetensiController@edit')->name('editKompetensi');
    Route::put('/kompetensi/{id}', 'KompetensiController@update')->name('updateKompetensi');
    Route::delete('/kompetensi/{id}', 'KompetensiController@destroy')->name('destroyKompetensi');
    
    //route Pertanyaan
    Route::get('/pertanyaan', 'PertanyaanController@index')->name('pertanyaan');
    // Route::get('/pertanyaan/:id', 'PertanyaanController@show')->name('detailPertanyaan');
    Route::get('/pertanyaan/create', 'PertanyaanController@create')->name('addPertanyaan');
    Route::post('/pertanyaan', 'PertanyaanController@store')->name('storePertanyaan');
    Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit')->name('editPertanyaan');
    Route::put('/pertanyaan/{id}', 'PertanyaanController@update')->name('updatePertanyaan');
    Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy')->name('destroyPertanyaan');
   
    //route Berkas
    Route::get('/berkas', 'BerkasController@index')->name('berkas');
    // Route::get('/berkas/:id', 'BerkasController@show')->name('detailBerkas');
    Route::get('/berkas/create', 'BerkasController@create')->name('addBerkas');
    Route::post('/berkas', 'BerkasController@store')->name('storeBerkas');
    Route::get('/berkas/{id}/edit', 'BerkasController@edit')->name('editBerkas');
    Route::put('/berkas/{id}', 'BerkasController@update')->name('updateBerkas');
    Route::delete('/berkas/{id}', 'BerkasController@destroy')->name('destroyBerkas');

    //route berita
    // Route::get('/berita', 'BeritaController@index')->name('berita');
    // Route::get('/berita/:id', 'BeritaController@show')->name('detailBerita');
    // Route::get('/berita/create', 'BeritaController@create')->name('addBerita');
    // Route::post('/berita', 'BeritaController@store')->name('storeBerita');
    // Route::get('/berita/:id/edit', 'BeritaController@edit')->name('editBerita');
    // Route::put('/berita/:id', 'BeritaController@update')->name('updateBerita');
    // Route::delete('/berita/:id', 'BeritaController@destroy')->name('destroyBerita');

    //route tracer
    Route::get('/tracer', 'TracerController@index')->name('tracer');
    Route::get('/tracer/create', 'TracerController@create')->name('addTracer');
    Route::get('/tracer/{id}', 'TracerController@show')->name('detailTracer');
    Route::post('/tracer', 'TracerController@store')->name('storeTracer');
    Route::delete('/tracer/:id', 'TracerController@destroy')->name('destroyTracer');
   
});

//usergroup dosen
Route::group(['middleware'=>['web','auth','usergroup_id:2']],
function()
{
    Route::get('/unduhberkasdosen', 'BerkasController@dosenUnduh')->name('berkasDosen');
    

});

//usergroup mahasiswa
Route::group(['middleware'=>['web','auth','usergroup_id:3']],
function()
{
    //route unduh berkas
    Route::get('/unduhberkas', 'UnduhController@index')->name('berkas.index');

    // route isi kuisioner
    Route::get('/kuisioner', 'JawabkuisionerController@index')->name('kuisioner');
    Route::get('/kuisioner/create', 'JawabkuisionerController@create')->name('addKuisioner');
    Route::post('/kuisioner', 'JawabkuisionerController@store')->name('storeKuisioner');

});


