<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\User;
use Hash;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (User::isAdmin()) {
            return view('admin.dashboard.index.mainadmin');
        }

        if (User::isDosen()) {
            return view('admin.dashboard.index.maindsn');
        }
        if (User::isAlumni()) {
            return view('admin.dashboard.index.mainalumni');
        }
        if (User::isMahasiswa()) {
            return view('admin.dashboard.index.mainmhs');
        }
        
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }

    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
        // The passwords matches
        return redirect()->back()->with("error","Password anda saat ini tidak sesuai dengan yang anda inputkan. Silahkan coba lagi.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
        //Current password and new password are same
        return redirect()->back()->with("error","Password baru tidak boleh sama dengan password anda saat ini. Silahkan pilih password yang berbeda.");
        }
        $validatedData = $request->validate([
        'current-password' => 'required',
        'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password berhasil diperbarui !");
    }
        
}
