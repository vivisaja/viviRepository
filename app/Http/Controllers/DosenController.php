<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Dosen;
use App\User;
use Validator;

use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use DB;
use Illuminate\Http\Response;


class DosenController extends Controller
{
    const USERGROUP_ID_ADMIN = 1;
    const USERGROUP_ID_DOSEN = 2;
    const USERGROUP_ID_MAHASISWA = 3;

    public function __construct()
  {
    //$this->middleware('auth');
   // $this->middleware('auth:dosen');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::all();
        $user = User::all();

        $dataDosen = Dosen::select(DB::raw(
            "dosen.id, nama, nidn, email"))
            ->join('users', 'user_id', '=', 'users.id')
            ->orderBy(DB::raw("dosen.id"))
            ->get();
        
        $data = array ('dosen' => $dataDosen);
        return view ('admin.dashboard.dosen.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dosen = Dosen::all();
        $user = User::all();
        return view ('admin.dashboard.dosen.form',compact('dosen','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nidn.required' => 'NIDN/NIK dibutuhkan',
            'nidn.unique' => 'NIDN/NIK sudah digunakan',
            'nama.required' => 'Nama dibutuhkan',
            'email.required' => 'Email dibutuhkan',
            'email.email' => 'Format email salah',
            'email.unique' => 'Email sudah digunakan',
        ];

        $rules = [
            'nidn' => 'required|unique:dosen',
            'nama' => 'required',
            'email' => 'required|email|unique:users',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $user = new User();
        $user->usergroup_id = self::USERGROUP_ID_DOSEN;
        $user->nama = $request->nama;
        $user->username = $request->nidn;
        $user->email = $request->email;
        $user->password = Hash::make($request->nidn);
        $user->save();

        $user_id = $user->id;

        $dosen = new Dosen();
        $dosen->nidn = $request->nidn;
        $dosen->user_id = $user_id;
        $dosen->save();

        return redirect()->route('dosen')
        ->with('success','Dosen Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen = Dosen::where('id',$id)->first();

        return view('admin.dashboard.dosen.formEdit',compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dosen = Dosen::find($id);
        $user_id = $dosen->user_id;
        $user = User::find($user_id);

        $user->update([
            'email' => ''
        ]);
        

        $input = $request->all();
        $messages = [
            'nama.required' => 'nama dibutuhkan',
            'email.required' => 'email dibutuhkan',
            'email.email' => 'format email salah',
            'email.unique' => 'email sudah digunakan',
        ];

        $rules = [
            'nama' => 'required',
            'email' => 'required|email|unique:users',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }
        
        $dosen->update($request->all());
        
        $user->update($request->all());
        

            return redirect()->route('dosen')
                        ->with('success','Data Dosen Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::find($id);
        $user_id = $dosen->user_id;
        $dosen->delete();

        $user = User::find($user_id);
        $user->delete();

        return redirect('dosen')
        ->with('success','Dosen Berhasil Dihapus');
    }
}
