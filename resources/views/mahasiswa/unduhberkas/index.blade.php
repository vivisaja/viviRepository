@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Unduh Berkas</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Unduh Berkas</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
  <div class="row">
    <div class="col-md-12">
        <div class="box box-primary" >
          <div class="box-body no-padding">
              <table id="dataBerkas" class="table table-bordered table-hover">
              <thead>
                <tr>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">Nama Berkas</th>
                <th style="text-align: center;">Aksi</th>
              </tr>
              </thead>
              @php $no = 1; @endphp
              <?php foreach ($berkas as $berkas): ?>
              <tbody>
                  <tr>
                  <td style="text-align: center;">{{$no++}}</td>
                  <td>{{$berkas->nama}}</td>
                  <td style="text-align: center; width:8%">
                      <a href="{{ Storage::url($berkas->url_file) }}" title="{{ $berkas->nama}}">
                      <span class="label label-info"><i class="fa fa-download">Unduh</i></span></a> 
                    </td>
                </tr>
                </tbody>
              <?php endforeach ?>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
  </div><!-- /.row (main row) -->
  </form>
            
  
@endsection


