@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Berkas</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Berkas</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
  <div class="row">
    <div class="col-md-12">
        <div class="box box-primary" >
          <div class="box-header">
            <h3 class="box-title">Data Berkas 
                <a href="{{{ route('addBerkas') }}}" class="btn btn-success btn-flat btn-sm" data-toggle="modal" title="Tambah"><i class="fa fa-plus"></i></a></h3>
          </div>
          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif
          <div class="box-body no-padding">
              <table id="dataBerkas" class="table table-bordered table-hover">
              <thead>
                <tr>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">Nama Berkas</th>
                {{--  <th style="text-align: center;">URL File</th>  --}}
                <th style="text-align: center;">Status Berkas</th>
                <th style="text-align: center;" colspan="3">Aksi</th>
              </tr>
              </thead>
              @php $no = 1; @endphp
              <?php foreach ($berkas as $berkas): ?>
              <tbody>
                  <tr>
                  <td style="text-align: center;">{{$no++}}</td>
                  <td>{{$berkas->nama}}</td>
                  {{--  <td>{{$berkas->url_file}}</td>  --}}
                  <td style="text-align: center;">{{ $berkas->status==1 ? 'Khusus' : 'Umum' }}</td>                  
                  
                  <td style="text-align: right; width:8%">
                      <a href="{{ Storage::url($berkas->url_file) }}" title="{{ $berkas->nama}}">
                      <span class="label label-info"><i class="fa fa-download">Unduh</i></span></a> 
                    </td>
                  <td style="text-align: center; width:6%">
                    <a href="{{ route('editBerkas',$berkas->id)}}">
                    <span class="label label-warning"><i class="fa fa-pencil">Edit</i></span></a> 
                  </td>
                <td style="width:8%">
                    <form action="{{ route('destroyBerkas', $berkas->id)}}" method="POST" onclick="return confirm('Apakah anda yakin akan menghapus berkas ?')">
                      @csrf
                      @method("DELETE")
                      <button type="submit" class="label label-danger"><i class="fa fa-trash">Hapus</i></button>
                    </form>
                </td>
                </tr>
                </tbody>
              <?php endforeach ?>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
  </div><!-- /.row (main row) -->
  </form>
            
@endsection


