@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Mata Kuliah</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Mata Kuliah</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
  <div class="row">
    <div class="col-md-8">
        <div class="box box-primary" >
          <div class="box-header">
            <h3 class="box-title">Data Mata Kuliah 
                <a href="{{{ route('addMatkul') }}}" class="btn btn-success btn-flat btn-sm" data-toggle="modal" title="Tambah"><i class="fa fa-plus"></i></a></h3>
          </div>
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif
          <div class="box-body no-padding">
            <table id="dataMatkul" class="table table-bordered table-hover">
              <thead>
                  <tr>
                    <th style="text-align: center;">ID</th>
                    <th style="text-align: center;">Mata Kuliah</th>
                    <th style="text-align: center;">Jumlah SKS</th>
                    <th style="text-align: center;" colspan="2">Aksi</th>
                  </tr>
              </thead>
              <tbody>
                  @php $no = 1; @endphp
                <?php foreach ($matkul as $matkul): ?>
                <tr>
                  <td style="text-align: center;">{{ $no++ }}</td>
                  <td>{{ $matkul->nama }}</td>
                  <td style="text-align: center;">{{ $matkul->sks }}</td>
                  <td style="text-align: right">
                      <a href="{{ route('editMatkul',$matkul->id)}}">
                      <span class="label label-warning"><i class="fa fa-pencil">Edit</i></span></a> 
                  </td>
                  <td>
                      <form action="{{ route('destroyMatkul', $matkul->id)}}" method="POST" onclick="return confirm('Apakah anda yakin akan menghapus Mata Kuliah ?')">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="label label-danger"><i class="fa fa-trash">Hapus</i></button>
                      </form>
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>

            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
  </div><!-- /.row (main row) -->
  </form>
            
@endsection
@section('script')
    <script src="{{ URL::asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
      $(function () {
        $('#dataMatkul').DataTable({"pageLength": 50});
      });
    </script>
@endsection

