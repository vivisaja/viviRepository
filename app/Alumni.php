<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $table ="alumni";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'mahasiswa_id','tahun_lulus','bidang_pekerjaan','pekerjaan',
        'tempat_bekerja','email_instansi','lama_bekerja','masa_tunggu',
    ];
  
    public function mahasiswa(){
        return $this->belongsTo('App\Mahasiswa');
    }
}
