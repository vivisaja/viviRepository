<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Mahasiswa;
use App\Alumni;

use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use DB;
use Illuminate\Http\Response;


use App\Repositories\RUser;

class MahasiswaController extends Controller
{
    const USERGROUP_ID_ADMIN = 1;
    const USERGROUP_ID_DOSEN = 2;
    const USERGROUP_ID_MAHASISWA = 3;
    const DEFAULT_NULL_ALUMNI = 'null';
    const DEFAULT_INT_ALUMNI = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
	{
		$this->middleware('auth');
	}
    public function index()
    {
        $mahasiswa = Mahasiswa::all();
        $user = User::all();

        $dataMahasiswa = Mahasiswa::select(DB::raw(
            "mahasiswa.id, nama, nim, angkatan, email, is_alumni"))
            ->join('users', 'user_id', '=', 'users.id')
            ->orderBy(DB::raw("mahasiswa.id"))
            ->get();
        
        $data = array ('mahasiswa' => $dataMahasiswa);
        return view ('admin.dashboard.mahasiswa.index',$data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mahasiswa = Mahasiswa::all();
        $user = User::all();
        return view ('admin.dashboard.mahasiswa.form',compact('mahasiswa','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nim.required' => 'nim dibutuhkan',
            'nim.unique' => 'nim sudah digunakan',
            'nama.required' => 'nama dibutuhkan',
            'angkatan.required' => 'angkatan dibutuhkan',
            'email.required' => 'email dibutuhkan',
            'email.email' => 'format email salah',
            'email.unique' => 'email sudah digunakan',
            'is_alumni.required' => 'status belum dipilih',
        ];

        $rules = [
            'nim' => 'required|unique:mahasiswa',
            'nama' => 'required',
            'angkatan' => 'required',
            'email' => 'required|email|unique:users',
            'is_alumni' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $user = new User();
        $user->usergroup_id = self::USERGROUP_ID_MAHASISWA;
        $user->nama = $request->nama;
        $user->username = $request->nim;
        $user->email = $request->email;
        $user->password = Hash::make($request->nim);
        $user->save();

        $user_id = $user->id;

        $mhs = new Mahasiswa();
        $mhs->nim = $request->nim;
        $mhs->user_id = $user_id;
        $mhs->angkatan = $request->angkatan;
        $mhs->is_alumni = $request->is_alumni;
        $mhs->save();

        $mahasiswa_id = $mhs->id;
        // menyimpan default null ke table alumni
        $alumni = new Alumni();
        $alumni->mahasiswa_id = $mahasiswa_id;
        $alumni->tahun_lulus = self::DEFAULT_NULL_ALUMNI;
        $alumni->bidang_pekerjaan = self::DEFAULT_NULL_ALUMNI;
        $alumni->pekerjaan = self::DEFAULT_NULL_ALUMNI;
        $alumni->tempat_bekerja = self::DEFAULT_NULL_ALUMNI;
        $alumni->email_instansi = self::DEFAULT_NULL_ALUMNI;
        $alumni->lama_bekerja = self::DEFAULT_INT_ALUMNI;
        $alumni->masa_tunggu = self::DEFAULT_INT_ALUMNI;
        $alumni->save();

        return redirect()->route('mahasiswa')
        ->with('success','Mahasiswa Berhasil Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = Mahasiswa::where('id',$id)->first();

        return view('admin.dashboard.mahasiswa.formEdit',compact('mahasiswa'));

     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $user_id = $mahasiswa->user_id;
        $user = User::find($user_id);

        $user->update([
            'email' => ''
        ]);
        

        $input = $request->all();
        $messages = [
            'nama.required' => 'nama dibutuhkan',
            'angkatan.required' => 'angkatan dibutuhkan',
            'email.required' => 'email dibutuhkan',
            'email.email' => 'format email salah',
            'email.unique' => 'email sudah digunakan',
            'is_alumni.required' => 'status belum dipilih',
        ];

        $rules = [
            'nama' => 'required',
            'angkatan' => 'required',
            'email' => 'required|email|unique:users',
            'is_alumni' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }
        
        $mahasiswa->update($request->all());
        
        $user->update($request->all());
        

            return redirect()->route('mahasiswa')
                        ->with('success',' Data mahasiswa Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $user_id = $mahasiswa->user_id;
        $mahasiswa_id = $mahasiswa->id;
        $alumni = Alumni::find($mahasiswa_id);
        $alumni->delete();
        $mahasiswa->delete();

        

        $user = User::find($user_id);
        $user->delete();

        return redirect('mahasiswa')
        ->with('success','Mahasiswa Berhasil Dihapus');
    }
}
