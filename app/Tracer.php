<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracer extends Model
{
    protected $table = "tracer";
    public $timestamps =true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'admin_id','nm_instansi','nm_responden','jabatan_res','alamat','jml_karyawan',
         'jml_karyawan_alumni','rata_masa_kerja_alumni','penghasilan_alumni',
         'jabatan_alumni','kerjasama','disiplin','etika','keuletan','kemampuan_teori',
         'kemampuan_praktik','rasa_pd','ketelitian','kreatifitas','kepemimpinan',
         'tanggung_jawab','keunggulan_alumni','kelemahan_alumni','kualitas_alumni',
         'kemampuan_alumni','saran_alumni','saran_pif',
    ];
}
