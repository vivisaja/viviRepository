<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{route ('home')}}">
            <i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-book"></i> <span>Data Master</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route ('dosen')}}"><i class="fa fa-circle-o text-aqua"></i>Data Dosen</a></li>
            <li><a href="{{route ('mahasiswa')}}"><i class="fa fa-circle-o text-aqua"></i>Data Mahasiswa</a></li>
            <li><a href="{{route ('matkul')}}"><i class="fa fa-circle-o text-aqua"></i>Data Mata Kuliah</a></li>
            <li><a href="{{route ('kompetensi')}}"><i class="fa fa-circle-o text-aqua"></i>Data Kompetensi</a></li>
            <li><a href="{{route ('pertanyaan')}}"><i class="fa fa-circle-o text-aqua"></i>Data Pertanyaan</a></li>
            <li><a href="{{route ('tracer')}}"><i class="fa fa-circle-o text-aqua"></i>Data Tracer Study</a></li>
            <li><a href="{{route ('berkas')}}"><i class="fa fa-circle-o text-aqua"></i>Data Berkas PJM</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-pie-chart"></i> <span>Laporan</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i>Evaluasi Perkuliahan</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i>Tracer Study</a></li>
          </ul>
        </li>
      </ul>
    </section>
  </aside>