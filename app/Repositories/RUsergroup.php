<?php 
namespace App\Repositories;
use App\Usergroups;

class RUsergroup{
    public static function runSeeder ($data){
        for ($i=0; $i < count($data) ; $i++) { 
            $item = $data[$i];
            $usergroup = new Usergroups ();
            $usergroup->id = $item['id'];
            $usergroup->name = $item['name'];
            $usergroup->information = $item['information'];
            $usergroup->save();
        }
    }
}