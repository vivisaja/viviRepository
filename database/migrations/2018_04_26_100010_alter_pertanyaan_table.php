<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function($table) {
            $table-> index ('admin_id');
            $table-> foreign ('admin_id')-> references ('id')-> on ('admin');
        });

        Schema::table('pertanyaan', function($table) {
            $table-> index ('kompetensi_id');
            $table-> foreign ('kompetensi_id')-> references ('id')-> on ('kompetensi');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function($table) {
            $table-> dropForeign (['admin_id']);
            $table-> dropIndex (['admin_id']);
        });

        Schema::table('pertanyaan', function($table) {
            $table-> dropForeign (['kompetensi_id']);
            $table-> dropIndex (['kompetensi_id']);
        });
        
    }
}
