<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Kompetensi;
use Validator;

class KompetensiController extends Controller
{
    const USER_ID_ADMIN = 1;

    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        $kompetensi = Kompetensi::all();
        return view ('admin.dashboard.kompetensi.index', compact('kompetensi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.dashboard.kompetensi.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'kompetensi.required' => 'Isian Tidak Boleh Kosong !',
        ];

        $rules = [
            'kompetensi' => 'required|max:60'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $kompetensi = new Kompetensi();
        $kompetensi->admin_id = self::USER_ID_ADMIN;
        $kompetensi->kompetensi = $request->kompetensi;
        
        if(! $kompetensi->save())
            App::abort(500);

        return redirect()->route('kompetensi')
        ->with('success','Kompetensi Berhasil Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kompetensi = Kompetensi::find($id);
        return view('admin.dashboard.kompetensi.formEdit', compact('kompetensi','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kompetensi = Kompetensi::find($id);
        $input = $request->all();
        $messages = [
            'kompetensi.required' => 'Isian Tidak Boleh Kosong !',
        ];

        $rules = [
            'kompetensi' => 'required|max:60'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }
        $kompetensi->update($request->all());

        return redirect()->route('kompetensi')
                        ->with('success','Kompetensi Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kompetensi = Kompetensi::find($id);
        $kompetensi->delete();
  
        return redirect('kompetensi')
        ->with('success','Kompetensi Berhasil Dihapus');
    }
}
