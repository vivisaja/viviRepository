@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Data Pertanyaan</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Data Pertanyaan</li>
          </ol>
@stop
@section('content')
<div>
  <!-- Horizontal Form -->
  <div class="box box-info">
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storePertanyaan') }}">
      @csrf
      <div class="box-body">
          <div class="form-group ">
            <label for="" class="col-sm-2 control-label">Kompetensi<span class="required">*</span></label>
              <div class="col-sm-10 has-error">
                <select class="form-control" name="kompetensi_id">
                  <option value=""> Silahkan Pilih</option>
                  @foreach ($kompetensi as $kompetensi)
                  <option value="{{ $kompetensi->id }}">
                      {{$kompetensi->kompetensi}}</option>
                  @endforeach
                </select>
                @if ($errors->has('kompetensi_id'))
                  <span class="help-block">
                    <font style="color:crimson"> {{ $errors->first('kompetensi_id') }}</font>                                
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">Isi Pertanyaan<span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control{{ $errors->has('pertanyaan') ? ' is-invalid' : '' }}" name="pertanyaan" id="pertanyaan" placeholder="Isi Pertanyaan" value="{{ old('pertanyaan') }}">
                  @if ($errors->has('pertanyaan'))
                    <span class="invalid-feedback">
                      <font style="color:crimson"> {{ $errors->first('pertanyaan') }}</font>                                
                    </span>
                  @endif
                </div>
              </div>               
        

      <!-- /.box-body -->
      <div class="box-footer" style="width: 59%;">
          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                <a href="{{ route('pertanyaan') }}" class="btn btn-primary" type="button">Kembali</a>
                <button type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
          </div>
        </div>
    </form>
  </div>
@endsection

