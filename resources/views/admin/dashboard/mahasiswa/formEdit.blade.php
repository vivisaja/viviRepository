@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Data Mahasiswa</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Data Mahasiswa</li>
          </ol>
@stop
@section('content')
<div>
    <!-- Horizontal Form -->
  <div class="box box-info">
      @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
      @endif
    
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('updateMahasiswa', $mahasiswa->id) }}">
      @csrf
      @method("PUT")
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">NIM<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nim" id="nim" placeholder="Ex. 140631100085" value="{{ $mahasiswa->nim }}" disabled="true">
           </div>
        </div>
        
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Ex. Vivi Oktavia" value="{{ $mahasiswa->user->nama }}">
            </div>
        </div> 
      
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Email<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="email" id="email" placeholder="Ex. vivisaja@gmail.com" value="{{ $mahasiswa->user->email }}">
            </div>
        </div>

        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Angkatan<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="angkatan" id="angkatan" placeholder="Ex. 2014" value="{{ $mahasiswa->angkatan }}">
            </div>
        </div>

        <div class="form-group ">
          <label for="" class="col-sm-2 control-label">Status<span class="required">*</span></label>
            <div class="col-sm-10 has-error">
              <select class="form-control" name="is_alumni">
                <option value="">Silahkan Pilih</option>
                <option value= "{{ $mahasiswa->is_alumni}}" {{$mahasiswa->is_alumni == "0" ? 'selected' : ''}}>Aktif</option>
                <option value= "{{ $mahasiswa->is_alumni}}" {{$mahasiswa->is_alumni == "1" ? 'selected' : ''}}>Alumni</option>
              </select>
              </div>
        </div>

          <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                  <a href="{{ route('mahasiswa') }}" class="btn btn-primary" type="button">Kembali</a>
                  <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
              </div>
            </div>
          </div>
    </form>
  </div>
</div>
@endsection


