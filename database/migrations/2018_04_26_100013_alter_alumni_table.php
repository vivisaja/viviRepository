<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAlumniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alumni', function($table) {
            $table-> index ('mahasiswa_id');
            $table-> foreign ('mahasiswa_id')-> references ('id')-> on ('mahasiswa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alumni', function($table) {
            $table-> dropForeign (['mahasiswa_id']);
            $table-> dropIndex (['mahasiswa_id']);
        });
    }
}
