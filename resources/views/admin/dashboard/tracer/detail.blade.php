<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tracer Study</title>
</head>
<body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;}
        .tgg  {border-collapse:collapse;border-spacing:0;width: 100%; }
    </style>
    <div>
        <img src="admin\dist\img\header_tracer.PNG" height="80%" width="100%">
    </div>
    <div style="font-family:Arial; font-size:12px;">
        <center><h2>KUESIONER PENELUSURAN LULUSAN (TRACER STUDY) <br>
            PROGRAM STUDI PENDIDIKAN INFORMATIKA (PIF)
        </h2></center>	
    </div>
    <table  class="tgg">
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="4">
                   <u><b>Instrumen Pengguna Lulusan/Alumni PIF</b></u>
            </td>
        </tr>
        <tr>
            <td width="5%"> 1.</td>
            <td width="50%"> Nama Instansi </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->nm_instansi}}</td>
        </tr>
        <tr>
            <td width="5%"> 2. </td>
            <td width="50%"> Nama Responden </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->nm_responden}}</td>
        </tr>
        <tr>
            <td width="5%"> 3. </td>
            <td width="50%"> Jabatan Responden </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->jabatan_res}}</td>
        </tr>
        <tr>
            <td width="5%"> 4. </td>
            <td width="50%"> Alamat Instansi </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->alamat}}</td>
        </tr>
        <tr>
            <td width="5%"> 5. </td>
            <td width="50%"> Jumlah Karyawan Seluruhnya </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->jml_karyawan}} Orang</td>
        </tr>
        <tr>
            <td width="5%"> 6. </td>
            <td width="50%"> Jumlah Karyawan Alumni Prodi PIF UTM  </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->jml_karyawan_alumni}} Orang</td>
        </tr>
        <tr>
            <td width="5%"> 7. </td>
            <td width="50%"> Rata-rata Masa Kerja Karyawan Alumni Prodi PIF  </td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->rata_masa_kerja_alumni}} Tahun</td>
        </tr>
        <tr>
            <td width="5%"> 8. </td>
            <td width="50%"> Penghasilan per bulan Karyawan Alumni Prodi PIF </td>
            <td width="5%"> : </td>
            <td width="40%">Rp. {{$tracer->penghasilan_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 9. </td>
            <td width="50%"> Jabatan yang diduduki Alumni Prodi PIF UTM</td>
            <td width="5%"> : </td>
            <td width="40%">{{$tracer->jabatan_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 10. </td>
            <td colspan="3"> <b>Kemampuan Kerja</b></td>
        </tr>
    </table>
    <table class="tg">
        <tr>
            <th class="tg-3wr7" width="10%">No.</th>
            <th class="tg-3wr7" width="50%">Jenis Kemampuan</th>
            <th class="tg-3wr7" width="40%">Keterangan</th>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>1.</center></td>
            <td class="tg-rv4w">Kerjasama</td>
            <td class="tg-rv4w">
                    @if ($tracer->kerjasama==4)
                    Sangat Tinggi 
                    @elseif ($tracer->kerjasama==3)
                    Tinggi
                    @elseif ($tracer->kerjasama==2)
                    Rendah
                    @elseif ($tracer->kerjasama==1)
                    Sangat Rendah
                    @endif 
                </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>2.</center></td>
            <td class="tg-rv4w">Disiplin</td>
            <td class="tg-rv4w">
                @if ($tracer->disiplin==4)
                Sangat Tinggi 
                @elseif ($tracer->disiplin==3)
                Tinggi
                @elseif ($tracer->disiplin==2)
                Rendah
                @elseif ($tracer->disiplin==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>3.</center></td>
            <td class="tg-rv4w">Etika/Moral</td>
            <td class="tg-rv4w">
                @if ($tracer->etika==4)
                Sangat Tinggi 
                @elseif ($tracer->etika==3)
                Tinggi
                @elseif ($tracer->etika==2)
                Rendah
                @elseif ($tracer->etika==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>4.</center></td>
            <td class="tg-rv4w">Keuletan</td>
            <td class="tg-rv4w">
                @if ($tracer->keuletan==4)
                Sangat Tinggi 
                @elseif ($tracer->keuletan==3)
                Tinggi
                @elseif ($tracer->keuletan==2)
                Rendah
                @elseif ($tracer->keuletan==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>5.</center></td>
            <td class="tg-rv4w">Kemampuan Teori</td>
            <td class="tg-rv4w">
                @if ($tracer->kemampuan_teori==4)
                Sangat Tinggi 
                @elseif ($tracer->kemampuan_teori==3)
                Tinggi
                @elseif ($tracer->kemampuan_teori==2)
                Rendah
                @elseif ($tracer->kemampuan_teori==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>6.</center></td>
            <td class="tg-rv4w">Kemampuan Praktik</td>
            <td class="tg-rv4w">
                @if ($tracer->kemampuan_praktik==4)
                Sangat Tinggi 
                @elseif ($tracer->kemampuan_praktik==3)
                Tinggi
                @elseif ($tracer->kemampuan_praktik==2)
                Rendah
                @elseif ($tracer->kemampuan_praktik==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>7.</center></td>
            <td class="tg-rv4w">Rasa Percaya Diri</td>
            <td class="tg-rv4w">
                @if ($tracer->rasa_pd==4)
                Sangat Tinggi 
                @elseif ($tracer->rasa_pd==3)
                Tinggi
                @elseif ($tracer->rasa_pd==2)
                Rendah
                @elseif ($tracer->rasa_pd==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>8.</center></td>
            <td class="tg-rv4w">Ketelitian</td>
            <td class="tg-rv4w">
                @if ($tracer->ketelitian==4)
                Sangat Tinggi 
                @elseif ($tracer->ketelitian==3)
                Tinggi
                @elseif ($tracer->ketelitian==2)
                Rendah
                @elseif ($tracer->ketelitian==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>9.</center></td>
            <td class="tg-rv4w">Kreativitas</td>
            <td class="tg-rv4w">
                @if ($tracer->kreatifitas==4)
                Sangat Tinggi 
                @elseif ($tracer->kreatifitas==3)
                Tinggi
                @elseif ($tracer->kreatifitas==2)
                Rendah
                @elseif ($tracer->kreatifitas==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>10.</center></td>
            <td class="tg-rv4w">Kepemimpinan</td>
            <td class="tg-rv4w">
                @if ($tracer->kepemimpinan==4)
                Sangat Tinggi 
                @elseif ($tracer->kepemimpinan==3)
                Tinggi
                @elseif ($tracer->kepemimpinan==2)
                Rendah
                @elseif ($tracer->kepemimpinan==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
        <tr>
            <td class="tg-rv4w"><center>11.</center></td>
            <td class="tg-rv4w">Tanggung Jawab</td>
            <td class="tg-rv4w">
                @if ($tracer->tanggung_jawab==4)
                Sangat Tinggi 
                @elseif ($tracer->tanggung_jawab==3)
                Tinggi
                @elseif ($tracer->tanggung_jawab==2)
                Rendah
                @elseif ($tracer->tanggung_jawab==1)
                Sangat Rendah
                @endif
            </td>
        </tr>
    </table>
    <table  class="tgg">
        <tr>
            <td width="5%"> 11.</td>
            <td colspan="3">Menurut Bapak/Ibu, apa keunggulan dari alumni Prodi PIF UTM? </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td colspan="3">{{$tracer->keunggulan_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 12.</td>
            <td colspan="3">Menurut Bapak/Ibu, apa kelemahan/kekurangan dari alumni Prodi PIF UTM?</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td colspan="3">{{$tracer->kelemahan_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 13.</td>
            <td colspan="3">Menurut Bapak/Ibu, bagaimana kualitas dari alumni Prodi PIF UTM?</td>
        </tr>
        <tr><td width="5%"></td><td colspan="3">a. Meningkat</td></tr>
        <tr><td width="5%"></td><td colspan="3">b. Stagnan</td></tr>
        <tr><td width="5%"></td><td colspan="3">c. Menurun</td></tr>
        <tr>
             <td width="5%"></td>
            <td colspan="3">{{$tracer->kualitas_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 14.</td>
            <td colspan="3">Menurut Bapak/Ibu,kemampuan-kemampuan atau kompetensi apakah yang sangat penting
                untuk dibekalkan kepada alumni Prodi PIF UTM yang sesuai dengan dunia kerja
                Pendidikan Kejuruan :
            </td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td colspan="3">{{$tracer->kemampuan_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 15.</td>
            <td colspan="3">Apa saran Bapak/Ibu bagi upaya pemantapan kompetensi alumni Prodi PIF UTM?</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td colspan="3">{{$tracer->saran_alumni}}</td>
        </tr>
        <tr>
            <td width="5%"> 16.</td>
            <td colspan="3">Apa saran Bapak/Ibu terhadap lembaga (Prodi PIF UTM)?</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td colspan="3">{{$tracer->saran_alumni}}</td>
        </tr>
    </table>
    
</body>
</html>