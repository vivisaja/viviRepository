<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Validator;

use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use DB;
use Illuminate\Http\Response;

use App\Berita;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['berita'] = Berita::all();
        return view('admin.dashboard.berita.showberita', $data);
    }

    public function ajaxList(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $berita = Berita::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'berita.id',
            'berita.judul',
            'berita.isi_berita',
        ])
        ;
        $datatables = Datatables::of($berita)
            ->addColumn('aksi', function ($r) {
                $aksi_edit = '';
                $aksi_delete = '';
                $aksi_edit = '<a href="'.route('editBerita', $r->id).'" title="edit" style="color: #000;"><i class="fa fa-pencil"></i></a>';
                $aksi_delete = '<a href="#" title="delete" onclick="event.preventDefault(); btn_delete('.$r->id.')" style="color: #000;"><i class="fa fa-times"></i></a>';
                return $aksi_edit.' '.$aksi_delete;
            })
        ;

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

         return $datatables->make(true);
     }
    public function create()
    {
        return view ('admin.dashboard.berita.createberita');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validate = $this->handleRequest($request);
        if (!$validate['success']) {
         return response()->json(array(
             'success' => false,
             'errors' => $validate['errors'],
         ), 400); // 400 being the HTTP code for an invalid request.
        }

        try {
         Berita::create($request->all());
         $result['success'] = true;
         $result['url'] = route('berita');
         $result['message'] = 'Berita berhasil disimpan!';

        } catch (QueryException $e) {
         $result['success'] = false;
         $result['message'] = 'Gagal disimpan.';
        }
        return response()->json($result);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['dt'] = Berita::find($id);
         if ( ! $data['dt'] )
         {
             return redirect('/berita')
                 ->with('alert-class', 'alert-danger')
                 ->with('message', 'Data tidak ditemukan.');
         }
         $data['mode'] = 'edit';
         $data['method'] = 'PUT';
         $data['action'] = url('/berita/'.$id);
         $data['judul'] = 'Edit Berita';
         return view('createberita', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
         $validate = $this->handleRequest($request);
         if (!$validate['success']) {
             return response()->json(array(
                 'success' => false,
                 'errors' => $validate['errors'],
             ), 400); // 400 being the HTTP code for an invalid request.
         }

         $berita = Berita::findorfail($id);
         try {
             $berita->update($request->all());
             $result['success'] = true;
             $result['url'] = route('berita');
             $result['message'] = 'Berita berhasil diedit!';
         } catch (QueryException $e) {
             $result['success'] = false;
             $result['message'] = 'Gagal disimpan.';
         }
         return response()->json($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $berita = Berita::findorfail($id);
                $berita->delete();
                $result['stat'] = true;
                $result['message'] = 'berita berhasil dihapus.';
            
        } catch (QueryException $e) {
            $result['stat'] = false;
            $result['message'] = 'error';
        }
        return response()->json($result);
    }
}
