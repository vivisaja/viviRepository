<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Berkas as berkas;
use Validator;
use Storage;

class BerkasController extends Controller
{
    const USER_ID_ADMIN = 1;

    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berkas = Berkas::all();
        return view ('admin.dashboard.berkas.index', compact('berkas'));
    }

    public function dosenUnduh()
    {
        $berkas = Berkas::all();
        return view ('dosen.unduhberkas.index', compact('berkas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.dashboard.berkas.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nama.required' => 'Nama dibutuhkan',
            'url_file.required' => 'Berkas belum di Upload', 
            'status.required' => 'Status belum dipilih', 
        ];

        $rules = [
            'nama' => 'required|max:60',
            'url_file' => 'required|file|max:2000',
            'status' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $path = $request->file('url_file')->store('public/files');

        $berkas = new Berkas();
        $berkas->admin_id = self::USER_ID_ADMIN;
        $berkas->nama = $request->nama;
        $berkas->url_file = $path;
        $berkas->status = $request->status;
        
        
        if(! $berkas->save())
            App::abort(500);

        return redirect()->route('berkas')
        ->with('success','Berkas Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berkas = Berkas::find($id);
        return view ('admin.dashboard.berkas.formEdit', compact('berkas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $berkas = Berkas::find($id);
        $input = $request->all();
        $messages = [
            'nama.required' => 'Nama dibutuhkan',
            'url_file.required' => 'Berkas belum di Upload', 
            'status.required' => 'Status belum dipilih', 
        ];

        $rules = [
            'nama' => 'required|max:60',
            'url_file' => 'required|file|max:2000',
            'status' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        if ($berkas->url_file){
            Storage::delete($berkas->url_file);
        }

        $url_file = $request->file('url_file')->store('public/files');

        $berkas->nama = $request->get('nama');
        $berkas->url_file = $url_file;
        $berkas->status = $request->get('status');
        $berkas->save();

        return redirect()->route('berkas')
                        ->with('success','Berkas Berhasil Diperbarui');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berkas = Berkas::find($id);

        if ($berkas->url_file){
            Storage::delete($berkas->url_file);
        }
        
        $berkas->delete();
  
        return redirect('berkas')
        ->with('success','Berkas Berhasil Dihapus');
    }
}
