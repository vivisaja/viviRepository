@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Data Mata Kuliah</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Data Mata Kuliah</li>
          </ol>
@stop
@section('content')
          
  <!-- right column -->
<div>
  <!-- Horizontal Form -->
  <div class="box box-info">
    <!-- /.box-header -->
    <!-- form start -->
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storeMatkul') }}">
      @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" id="nama" placeholder="Ketikkan Nama Mata Kuliah" value="{{ old('nama') }}">
            @if ($errors->has('nama'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('nama') }}</font>                                
              </span>
            @endif
          </div>
        </div>
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Jumlah SKS<span class="required">*</span></label>
          <div class="col-sm-3">
            <input type="text" class="form-control{{ $errors->has('sks') ? ' is-invalid' : '' }}" name="sks" id="sks" placeholder="Jumlah SKS" value="{{ old('sks') }}">
            @if ($errors->has('sks'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('sks') }}</font>                                
              </span>
            @endif
          </div>
        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="width: 59%;">
          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                <a href="{{ route('matkul') }}" class="btn btn-primary" type="button">Kembali</a>
                <button type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
          </div>
        </div>
      <!-- /.box-footer -->
    </form>
  </div>
  <!-- /.box -->
        

@endsection

