<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Jawabkuisioner;
use App\Dosen;
use App\Matkul;
use App\User;
use App\Pertanyaan;
use App\Kompetensi;
use Validator;
use DB;

class JawabkuisionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['jawabkuisioner'] = Jawabkuisioner::all();
        // $data['users'] = User::all();
        // $data['dosen'] = Dosen::all();
        // $data['matkul'] = Matkul::all();
        return view ('mahasiswa.kuisioner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['jawabkuisioner'] = Jawabkuisioner::all();
        // $data['users'] = User::where('user_id','=','users.id')->get();
        $data['users'] = DB::table('users')
                        ->join('dosen', 'users.id', '=', 'dosen.user_id')
                        ->select('users.*','dosen.*')
                        ->get();
        $data['matkul'] = Matkul::all();
        $data['kompetensi'] = Kompetensi::with('pertanyaan')->get();

        return view ('mahasiswa.kuisioner.isi_kuisioner',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        $messages = [
            'tahun_ajar.required' => 'Tahun ajar dibutuhkan !',
            'semester.required' => 'semester dibutuhkan !',
            'dosen_id.required' => 'Dosen belum dipilih',
            'matkul_id.required' => 'Mata kuliah belum dipilih',
            'val'    => 'required|array',
            'val.*'    => 'required|in:1,2,3,4,5',
        ];

        $rules = [
            'tahun_ajar' => 'required',
            'semester' => 'required',
            'dosen_id' => 'required',
            'matkul_id' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }
        $arr = $request->except('_token');
        foreach ($arr as $key => $value) {

        $jawabkuisioner = new Jawabkuisioner();

        $newValue = json_encode($value['jawaban']);

        $jawabkuisioner->user_id = $request->user()->id;
        $jawabkuisioner->tahun_ajar = $request->tahun_ajar;
        $jawabkuisioner->semester = $request->semester;
        $jawabkuisioner->dosen_id = $request->dosen_id;
        $jawabkuisioner->matkul_id = $request->matkul_id;
        $jawabkuisioner->pertanyaan_id =$key;
        $jawabkuisioner->jawaban = $newValue;

        $answerArray[] = $jawabkuisioner;
        };
        
        if(! $jawabkuisioner->save())
            App::abort(500);

        return redirect()->route('kuisioner')
        ->with('success','Kuisioner Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
