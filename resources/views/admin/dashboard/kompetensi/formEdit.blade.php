@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Data Kompetensi</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Data Kompetensi</li>
          </ol>
@stop
@section('content')
        <div>
          <div class="box box-info">
            <!-- /.box-header -->
            @if ($errors->any())
              <div class="alert alert-danger">
                  <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif

            <!-- form start -->
            <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('updateKompetensi', $id) }}">
             @csrf
             @method ("PUT")
              <div class="box-body">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">ID<span class="required">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="" name="id" value="{{$kompetensi->id}}" disabled="true">
                    </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" name="kompetensi" value="{{$kompetensi->kompetensi}}" placeholder="Ketikkan Nama Kompetensi">
                  </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer" style="width: 59%;">
                  <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                        <a href="{{ route('kompetensi') }}" class="btn btn-primary" type="button">Kembali</a>
                        <button type="submit" class="btn btn-success">Perbarui</button>
                      </div>
                    </div>
                  </div>
                </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        

@endsection

