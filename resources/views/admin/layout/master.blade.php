<!DOCTYPE html>
{{--  <!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only. -->  --}}
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SI PJM | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- bootstrap 3.3.7 -->
  <link href=" {{URL::asset("admin/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">
  <!-- Font Awesome -->
  <link href="{{URL::asset("admin/bower_components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css">
  <!-- Ionicons -->
  <link href="{{URL::asset("admin/bower_components/Ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css">
  <!-- Theme style -->
  <link  href="{{URL::asset("admin/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css">
  <link href="{{URL::asset("admin/dist/css/skins/skin-blue.min.css") }}" rel="stylesheet" type="text/css">

  </head>
  <body class ="skin-blue">
  <div class="wrapper">

  <!-- Main Header -->
  <!--memasukkan file header.blade.php yang ada di folder admin/include/-->
  @include('admin.include.header')

  <!-- Left side column. contains the logo and sidebar -->
  @if(Auth::user()->usergroup_id==1)
    @include('admin.include.sidebar')
  @elseif(Auth::user()->usergroup_id==2)
    @include('admin.include.sidebardsn')
  @elseif(Auth::user()->usergroup_id==3)
    @if(Auth::user()->mahasiswa->is_alumni==0)
      @include('admin.include.sidebarmhs')
    @elseif(Auth::user()->mahasiswa->is_alumni==1)
      @include('admin.include.sidebaralumni')
    @endif
  @endif
  
  {{--  @if (User::isAdmin())
    @include('admin.include.sidebar')
  @elseif (User::isDosen())
    @include('admin.include.sidebardsn')
  @elseif (User::isMahasiswa())
    @include('admin.include.sidebarmhs')
  @endif  --}}

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @yield('breadcrump')
        </section>

        <!-- Main content -->
        <section class="content">
          @yield('content')
        </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
       

  <!-- Main Footer -->
  @include('admin.include.footer')

  <!-- Control Sidebar -->

<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset("admin/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset("admin/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("admin/dist/js/adminlte.min.js") }}" type="text/javascript"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>