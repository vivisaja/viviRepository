<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Pertanyaan;
use App\Kompetensi;
use Validator;

use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use DB;
use Illuminate\Http\Response;

class PertanyaanController extends Controller
{
    const USER_ID_ADMIN = 1;

    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        $kompetensi = Kompetensi::orderBy('id')->get();

        $dataPertanyaan = Pertanyaan::select(DB::raw(
            "pertanyaan.id, pertanyaan, kompetensi"))
            ->join('kompetensi', 'kompetensi_id', '=', 'kompetensi.id')
            ->orderBy(DB::raw("pertanyaan.id"))
            ->get();
        
        $data = array ('pertanyaan' => $dataPertanyaan);
        return view ('admin.dashboard.pertanyaan.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pertanyaan'] = Pertanyaan::all();
		$data['kompetensi'] = Kompetensi::all();
        return view ('admin.dashboard.pertanyaan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'kompetensi_id.required' => 'Isian Tidak Boleh Kosong !',
            'pertanyaan.required' => 'Isian Tidak Boleh Kosong !', 
        ];

        $rules = [
            'kompetensi_id' => 'required',
            'pertanyaan' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $pertanyaan = new Pertanyaan();
        $pertanyaan->admin_id = self::USER_ID_ADMIN;
        $pertanyaan->kompetensi_id = $request->kompetensi_id;
        $pertanyaan->pertanyaan = $request->pertanyaan;
        
        if(! $pertanyaan->save())
            App::abort(500);

        return redirect()->route('pertanyaan')
        ->with('success','Pertanyaan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kompetensi = Kompetensi::all();
        return view('admin.dashboard.pertanyaan.formEdit', compact('pertanyaan','kompetensi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $input = $request->all();
        $messages = [
            'pertanyaan.required' => 'Isian Tidak Boleh Kosong !', 
            'kompetensi_id.required' => 'Isian Tidak Boleh Kosong !',
        ];

        $rules = [
            'pertanyaan' => 'required',
            'kompetensi_id' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $pertanyaan->update($request->all());

        return redirect()->route('pertanyaan')
                        ->with('success','Pertanyaan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->delete();
  
        return redirect('pertanyaan')
        ->with('success','Pertanyaan Berhasil Dihapus');
    }
}
