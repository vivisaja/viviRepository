@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Pertanyaan</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Pertanyaan</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
  <div class="row">
    <div class="col-md-12">
        <div class="box box-primary" >
          <div class="box-header">
            <h3 class="box-title">Data Pertanyaan
                <a href="{{{ route('addPertanyaan') }}}" class="btn btn-success btn-flat btn-sm" data-toggle="modal" title="Tambah"><i class="fa fa-plus"></i></a></h3>
          </div>
          @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
          @endif
          <div class="box-body no-padding">
            <table id="dataPertanyaan" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">Pertanyaan</th>
                <th style="text-align: center;">Kompetensi</th>
                <th style="text-align: center;" colspan="2">Aksi</th>
              </tr>
              </thead>
              @php $no = 1; @endphp
              <?php foreach ($pertanyaan as $pertanyaan): ?>
              <tbody>
              <tr>
                <td style="text-align: center;">{{$no++}}</td>
                <td>{{$pertanyaan->pertanyaan}}</td>
                <td>{{$pertanyaan->kompetensi}}</td>
                
                <td style="text-align: right">
                    <a href="{{ route('editPertanyaan',$pertanyaan->id)}}">
                    <span class="label label-warning"><i class="fa fa-pencil">Edit</i></span></a> 
                </td>
                <td>
                    <form action="{{ route('destroyPertanyaan', $pertanyaan->id)}}" method="POST" onclick="return confirm('Apakah anda yakin akan menghapus Pertanyaan ?')">
                      @csrf
                      @method("DELETE")
                      <button type="submit" class="label label-danger"><i class="fa fa-trash">Hapus</i></button>
                    </form>
                </td>
              </tr>
              <?php endforeach ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
  </div><!-- /.row (main row) -->
  </form>
            
@endsection
@section('script')
    <script src="{{ URL::asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
      $(function () {
        $('#dataPertanyaan').DataTable({"pageLength": 50});
      });
    </script>
@endsection


