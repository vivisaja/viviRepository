<?php

use Illuminate\Database\Seeder;

class RunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this-> call([
            UsergroupSeeder::class,
            UserSeeder::class,
            AdminSeeder::class
        ]);
    }
}
