@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Berkas</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Berkas</li>
          </ol>
@stop
@section('content')
          
    <!-- right column -->
<div>
    <!-- Horizontal Form -->
    <div class="box box-info">
    <!-- /.box-header -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- form start -->
    <form id="" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('updateBerkas', $berkas) }}">
        @csrf
        @method("PUT")
        <div class="box-body">
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Ketikkan Nama Berkas" value="{{ $berkas->nama }}">
                </div>
            </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">File input<span class="required">*</span></label>
            <div class="col-sm-10">
            <input type="file" id="url_file" name="url_file" value="{{ $berkas->url_file }}" required
            <p class="help-block"></p>
            </div>
        </div>

        <div class="form-group ">
            <label for="" class="col-sm-2 control-label">Status Berkas<span class="required">*</span></label>
                <div class="col-sm-10 has-error">
                <select class="form-control" name="status">
                    <option value="">Silahkan Pilih</option>
                    <option value= "{{ $berkas->status}}" {{$berkas->status == "0" ? 'selected' : ''}}>Umum</option>
                    <option value= "{{ $berkas->status}}" {{$berkas->status == "1" ? 'selected' : ''}}>Khusus</option>
                </select>
                </div>
            </div>
        <!-- /.box-body -->
        <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
            <div class="form-actions">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                <a href="{{ route('berkas') }}" class="btn btn-primary" type="button">Kembali</a>
                <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
            </div>
            </div>
        <!-- /.box-footer -->
    </form>
    </div>
    <!-- /.box -->
        

@endsection

