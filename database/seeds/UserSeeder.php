<?php

use Illuminate\Database\Seeder;

use App\Repositories\RUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'usergroup_id' => 1,
                'nama' => 'admin',
                'email' => 'admin@gmail.com',
                'username' => 'admin',
                'password' => 'admin'
            ],
            // [
            //     'usergroup_id' => 2,
            //     'nama' => 'Ishaq',
            //     'email' => 'Ishaq@gmail.com',
            //     'username' => 'ishaq',
            //     'password' => 'ishaq'
            // ],
            // [
            //     'usergroup_id' => 3,
            //     'nama' => 'vivi',
            //     'email' => 'vivi@gmail.com',
            //     'username' => 'vivi',
            //     'password' => 'vivi1996'
            // ],
        ];
        rUser::runSeeder($data);
    }
}
