@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tracer Study</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tracer Study</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
    <div class="row">
      <div class="col-md-12">
          <div class="box box-primary" >
            <div class="box-header">
              <h3 class="box-title">Data Tracer Study 
                  <a href="{{ route('addTracer') }}" class="btn btn-success btn-flat btn-sm"  title="Tambah"><i class="fa fa-plus"></i></a></h3>
            </div>
            @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                      <p>{{ $message }}</p>
                  </div>
            @endif
            <div class="box-body no-padding">
                <table id="dataTracer" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th style="text-align: center;">ID</th>
                  <th style="text-align: center;">Nama Instansi</th>
                  <th style="text-align: center;">Nama Responden</th>
                  <th style="text-align: center;">Aksi</th>
                </tr>
                </thead>
                @php $no = 1; @endphp
                <?php foreach ($tracer as $tracer): ?>
                <tbody>
                    <tr>
                    <td style="text-align: center;">{{$no++}}</td>
                    <td>{{$tracer->nm_instansi}}</td>
                    <td>{{$tracer->nm_responden}}</td>
                    <td style="width:10%; text-align: center">
                      <a href="{{ route('detailTracer',$tracer->id)}}">
                      <span class="label label-success"><i class="fa fa-list"> Detail</i></span></a> 
                  </td>
                  </tr>
                  </tbody>
                <?php endforeach ?>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
    </div><!-- /.row (main row) -->
    </form>            
@endsection


