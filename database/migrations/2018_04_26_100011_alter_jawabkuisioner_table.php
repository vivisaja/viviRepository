<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterJawabKuisionerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawabkuisioner', function($table) {
            $table-> index ('user_id');
            $table-> foreign ('user_id')-> references ('id')-> on ('users');
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> index ('dosen_id');
            $table-> foreign ('dosen_id')-> references ('id')-> on ('dosen');
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> index ('matkul_id');
            $table-> foreign ('matkul_id')-> references ('id')-> on ('matkul');
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> index ('pertanyaan_id');
            $table-> foreign ('pertanyaan_id')-> references ('id')-> on ('pertanyaan');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawabkuisioner', function($table) {
            $table-> dropForeign (['user_id']);
            $table-> dropIndex (['user_id']);
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> dropForeign (['dosen_id']);
            $table-> dropIndex (['dosen_id']);
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> dropForeign (['matkul_id']);
            $table-> dropIndex (['matkul_id']);
        });

        Schema::table('jawabkuisioner', function($table) {
            $table-> dropForeign (['pertanyaan_id']);
            $table-> dropIndex (['pertanyaan_id']);
        });
    }
}
