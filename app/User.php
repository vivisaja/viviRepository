<?php

namespace App;

//use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usergroup_id','nama','username', 'email', 'password',
    ];
    public function mahasiswa()
    {
    	return $this->hasOne('App\Mahasiswa');
    }

    public function dosen()
    {
    	return $this->hasOne('App\Dosen');
    }

    public function jawabkuisioner(){
        return $this->hasMany('App\Jawabkuisioner');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
