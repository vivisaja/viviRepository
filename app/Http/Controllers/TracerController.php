<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use File;
use Excel;
use PDF;
use App\Tracer;

class TracerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracer = Tracer::all();
        return view ('admin.dashboard.tracer.index', compact('tracer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.dashboard.tracer.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
 
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'admin_id' => $value->admin_id,   
                        'nm_instansi' => $value->nm_instansi,
                        'nm_responden' => $value->nm_responden,
                        'jabatan_res' => $value->jabatan_res,
                        'alamat' => $value->alamat,
                        'jml_karyawan' => $value->jml_karyawan,
                        'jml_karyawan_alumni' => $value->jml_karyawan_alumni,
                        'rata_masa_kerja_alumni' => $value->rata_masa_kerja_alumni,
                        'penghasilan_alumni' => $value->penghasilan_alumni,
                        'jabatan_alumni' => $value->jabatan_alumni,
                        'kerjasama' => $value->kerjasama,
                        'disiplin' => $value->disiplin,
                        'etika' => $value->etika,
                        'keuletan' => $value->keuletan,
                        'kemampuan_teori' => $value->kemampuan_teori,
                        'kemampuan_praktik' => $value->kemampuan_praktik,
                        'rasa_pd' => $value->rasa_pd,
                        'ketelitian' => $value->ketelitian,
                        'kreatifitas' => $value->kreatifitas,
                        'kepemimpinan' => $value->kepemimpinan,
                        'tanggung_jawab' => $value->tanggung_jawab,
                        'keunggulan_alumni' => $value->keunggulan_alumni,
                        'kelemahan_alumni' => $value->kelemahan_alumni,
                        'kualitas_alumni' => $value->kualitas_alumni,
                        'kemampuan_alumni' => $value->kemampuan_alumni,
                        'saran_alumni' => $value->saran_alumni,
                        'saran_pif' => $value->saran_pif,
                        ];
                    }
 
                    if(!empty($insert)){
 
                        $insertData = DB::table('tracer')->insert($insert);
                        if ($insertData) {
                            return redirect()->route('tracer')
                            ->with('success','Data berhasil ditambahkan');
                        }else {                        
                            return redirect()->route('tracer')
                            ->with('error','Gagal Memasukkan Data');
                        }
                    }
                }
                return back();
            }else {
                Session::flash('error', 'File bertipe '.$extension.'.!! Silahkan upload file bertipe xls yang valid..!!');
                return back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tracer = Tracer::find($id);
        view()->share ('tracer','id');
        $pdf =PDF::loadView('admin.dashboard.tracer.detail',compact ('tracer'));
        return $pdf->stream();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
