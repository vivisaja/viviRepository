<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawabkuisioner extends Model
{
    protected $table ="jawabkuisioner";
    public $timestamps =true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','dosen_id','matkul_id','pertanyaan_id','tahun_ajar','semester','jawaban'
    ];
  
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function Dosen(){
        return $this->belongsTo('App\Dosen');
    }

    public function Matkul(){
        return $this->belongsTo('App\Matkul');
    }

    public function Pertanyaan(){
        return $this->belongsTo('App\Pertanyaan');
    }


}
