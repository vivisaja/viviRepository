<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Matkul;
use Validator;

class MatkulController extends Controller
{
    const USER_ID_ADMIN = 1;

    public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $matkul = Matkul::all();
        return view ('admin.dashboard.matkul.index', compact('matkul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.dashboard.matkul.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nama.required' => 'Isian Tidak Boleh Kosong !',
            'sks.required' => 'Isian Tidak Boleh Kosong !', 
        ];

        $rules = [
            'nama' => 'required|max:60',
            'sks' => 'required',
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $matkul = new Matkul();
        $matkul->admin_id = self::USER_ID_ADMIN;
        $matkul->nama = $request->nama;
        $matkul->sks = $request->sks;
        
        if(! $matkul->save())
            App::abort(500);

        return redirect()->route('matkul')
        ->with('success','Mata Kuliah Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matkul = Matkul::find($id);
        return view('admin.dashboard.matkul.formEdit', compact('matkul','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $matkul = Matkul::find($id);
        $input = $request->all();
        $messages = [
            'nama.required' => 'Isian Tidak Boleh Kosong !',
            'sks.required' => 'Isian Tidak Boleh Kosong !', 
        ];

        $rules = [
            'nama' => 'required|max:60',
            'sks' => 'required'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            # Kembali kehalaman yang sama dengan pesan error
            return Redirect::back()->withErrors($validator)->withInput();

          # Bila validasi sukses
        }

        $matkul->update($request->all());

        return redirect()->route('matkul')
                        ->with('success','Mata Kuliah Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matkul = Matkul::find($id);
        $matkul->delete();
  
        return redirect('matkul')
        ->with('success','Mata Kuliah Berhasil Dihapus');
    }
}
