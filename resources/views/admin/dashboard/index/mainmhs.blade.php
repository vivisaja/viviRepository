@extends('admin.layout.master')
@section('breadcrump')
<h1>
            Dashboard
            <small>Mahasiswa</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard Mahasiswa</li>
          </ol>
@stop
<!-- main content -->
@section('content')
<div class="callout callout-info">
    <h4>Selamat Datang Mahasiswa !</h4>
    <p>Perhatikan hak akses apa saja yang anda miliki ketika menggunakan SI PJMPIF ini. </p>
</div>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Penting !!</h3>
        <div class="box-tools pull-right">
            <button data-original-title="Collapse" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
        </div>
    </div>

    <div style="display: block;" class="box-body">
      Hak akses yang anda miliki antara lain :
        <ul>
            <li>
              Mengubah username dan password</li>
            <li>
              Mengisi Kuisioner Perkuliahan</li>
            <li>
              Mengunduh Berkas Penjaminan Mutu</li>
        </ul>
    </div><!-- /.box-body -->
</div>         
@endsection

@section('script')



@endsection
