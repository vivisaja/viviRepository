<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kompetensi extends Model
{
    protected $table ="kompetensi";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'admin_id','kompetensi',
    ];

    public function Pertanyaan(){
        return $this->hasMany('App\Pertanyaan');
    }
}
