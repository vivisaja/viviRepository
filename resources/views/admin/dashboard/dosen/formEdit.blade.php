@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Data Dosen</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Data Dosen</li>
          </ol>
@stop
@section('content')
<div>
    <!-- Horizontal Form -->
  <div class="box box-info">
      @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div>
      @endif
    
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('updateDosen', $dosen->id) }}">
      @csrf
      @method("PUT")
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">NIDN/NIK<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nidn" id="nidn" placeholder="Ex. 199610182019042001" value="{{ $dosen->nidn }}" disabled="true">
           </div>
        </div>
        
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Ex. Vivi Oktavia" value="{{ $dosen->user->nama }}">
            </div>
        </div> 
      
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Email<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="email" id="email" placeholder="Ex. vivisaja@gmail.com" value="{{ $dosen->user->email }}">
            </div>
        </div>

          <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                  <a href="{{ route('dosen') }}" class="btn btn-primary" type="button">Kembali</a>
                  <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
              </div>
            </div>
          </div>
    </form>
  </div>
</div>
@endsection


