<?php
namespace App\Http\Middleware;

use Closure;
use App;
use Illuminate\Support\Facades\Auth as Auth;

class UsergroupMiddleware
{
    public function handle($request, Closure $next, $usergroup_id)
    {
    	$user = Auth::user();

        if ($user && $user->usergroup_id != $usergroup_id) {
            
            return App::abort(Auth::check() ? 403 : 401,
             Auth::check() ? 'Forbidden' : 'Unauthorized');
            
        }

        return $next($request);
    }
}