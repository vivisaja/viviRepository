<?php

use Illuminate\Database\Seeder;

use App\Repositories\RAdmin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 1,
                'jabatan' => 'Ketua'
            ],
            ];
            rAdmin::runSeeder($data);
    }
}
