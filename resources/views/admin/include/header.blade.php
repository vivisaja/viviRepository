<header class="main-header">

  <a href="home" class="logo">
    <span class="logo-mini"></span>
    <span class="logo-lg"><b>SI PJMPIF</b></span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
  {{--  <a href="#" class="sidebar-toggle1" data-toggle="push-menu" role="button">
      <span>a</span>
    </a>  --}}
  {{--  <a class="sidebar-toggle1">
      <span>Sistem Informasi Penjaminan Mutu Pendidikan Informatika</span>
  </a>  --}}
  
<!-- Control Sidebar Toggle Button -->
<div class="navbar-custom-menu">
   <ul class="nav navbar-nav">
     <!-- User Account Menu -->
     
     <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ strtoupper(Auth::user()->nama) }} </span>
        </a>
        <ul class="dropdown-menu">
          <!-- The user image in the menu -->
          <li class="user-header">
            <p>
              {{ strtoupper(Auth::user()->nama) }} 
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="{{route('showform')}}" class="btn btn-primary">Ubah Password</a>
            </div>
            <div class="pull-right">
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-primary">Logout</a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </div>
          </li>
        </ul>
      </li>
       <li>
         
        <!-- search form
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
              </span>
            </div>
          </form>
           <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
      </li>
  </ul>
</div>-->



      
</nav>
</header>