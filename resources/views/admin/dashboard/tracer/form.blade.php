@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Tracer Study</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Tracer Study</li>
          </ol>
@stop
@section('content')
           
<!-- right column -->
<div>
  <!-- Horizontal Form -->
  <div class="box box-info">
    <!-- /.box-header -->
    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif
    <!-- form start -->
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storeTracer') }}" enctype="multipart/form-data">
      @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">File input<span class="required">*</span></label>
          <div class="col-sm-3">
            <input type="file" id="file" name="file">
            <p class="help-block">Upload File xls Tracer Study</p>
          </div>
        </div>
      <!-- /.box-body -->
      <div class="box-footer" style="width: 59%;">
          <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                <a href="{{ route('tracer') }}" class="btn btn-primary" type="button">Kembali</a>
                <button type="submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
          </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        

@endsection

