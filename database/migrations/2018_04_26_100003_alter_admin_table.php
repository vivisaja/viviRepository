<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('admin', function($table) {
            $table-> index ('user_id');
            $table-> foreign ('user_id')-> references ('id')-> on ('users');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('admin', function($table) {
            $table-> dropForeign (['user_id']);
            $table-> dropIndex (['user_id']);
        });
        
    }
}
