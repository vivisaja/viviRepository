@extends('layouts.app')

@section('content')
<div class="container">

        <div class="row" style="margin-top:20px">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-5 col-md-offset-5 offset-md-3">
                {{--  <form role="form">  --}}
                <form method="POST" action="{{ route('login') }}" role="form">
                    @csrf
                    <fieldset style="align-content: center">
                        <h3><center>Sistem Informasi Penjaminan Mutu</h3></center>
                        <h3><center>Pendidikan Informatika</h3></center>
                        <hr class="colorgraph">
                        <div class="form-group row">
                                <label for="username" class="col-sm-4 col-form-label text-md-right">{{ __('Username') }}</label>
    
                                <div class="col-md-6">
                                    <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
    
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
    
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
    
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        <hr class="colorgraph">
                        <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-lg btn-success btn-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        
                    </fieldset>
                </form>
            </div>
        </div>
        
        </div>
@endsection
