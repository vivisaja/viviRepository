<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table ="dosen";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id','nidn'
    ];
    //

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function JawabKuisioner(){
        return $this->hasOne('App\JawabKuisioner');
    }
}