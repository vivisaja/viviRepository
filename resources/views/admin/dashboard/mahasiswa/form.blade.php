@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Tambah Data Mahasiswa</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Tambah Data Mahasiswa</li>
          </ol>
@stop
@section('content')
<div>
    <!-- Horizontal Form -->
  <div class="box box-info">
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('storeMahasiswa') }}">
      @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">NIM<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control{{ $errors->has('nim') ? ' is-invalid' : '' }}" name="nim" id="nim" placeholder="Ex. 140631100085" value="{{ old('nim') }}">
            @if ($errors->has('nim'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('nim') }}</font>                                
              </span>
            @endif
          </div>
        </div>
        
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Nama<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" id="nama" placeholder="Ex. Vivi Oktavia" value="{{ old('nama') }}">
            @if ($errors->has('nama'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('nama') }}</font>                                
              </span>
            @endif
          </div>
        </div> 
      
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Email<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Ex. vivisaja@gmail.com" value="{{ old('email') }}">
            @if ($errors->has('email'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('email') }}</font>                                
              </span>
            @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Angkatan<span class="required">*</span></label>
          <div class="col-sm-10">
            <input type="text" class="form-control{{ $errors->has('angkatan') ? ' is-invalid' : '' }}" name="angkatan" id="angkatan" placeholder="Ex. 2014" value="{{ old('angkatan') }}">
            @if ($errors->has('angkatan'))
              <span class="invalid-feedback">
                <font style="color:crimson"> {{ $errors->first('angkatan') }}</font>                                
              </span>
            @endif
          </div>
        </div>

        <div class="form-group ">
          <label for="" class="col-sm-2 control-label">Status<span class="required">*</span></label>
            <div class="col-sm-10 has-error">
              <select class="form-control" name="is_alumni">
                  <option value=""> Silahkan Pilih</option>
                  <option value="0"> Aktif</option>
                  <option value="1"> Alumni</option>
              </select>
              @if ($errors->has('is_alumni'))
                <span class="help-block">
                  <font style="color:crimson"> {{ $errors->first('is_alumni') }}</font>                                
                </span>
              @endif
            </div>
        </div>

          <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                  <a href="{{ route('mahasiswa') }}" class="btn btn-primary" type="button">Kembali</a>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </div>
          </div>
    </form>
  </div>
</div>
@endsection


