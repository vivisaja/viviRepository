<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{route('home')}}">
          <i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
        <li><a href="#"><i class="fa fa-pencil">
          </i> <span>Perbarui Data</span></a></li>
        <li><a href="{{route('berkas.index')}}"><i class="fa fa-download">
          </i> <span>Berkas Penjaminan Mutu</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>