<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table ="mahasiswa";
    public $timestamps =false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'nim','user_id','angkatan','is_alumni'
    ];
  
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function alumni(){
        return $this->hasOne('App\Alumni');
    }
    
}
