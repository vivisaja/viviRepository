<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMatkulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('matkul', function($table) {
            $table-> index ('admin_id');
            $table-> foreign ('admin_id')-> references('id')-> on ('admin');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('matkul', function($table) {
            $table-> dropForeign (['admin_id']);
            $table-> dropIndex (['admin_id']);
        });
        
    }
}
