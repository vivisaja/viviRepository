<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('users', function($table) {
            $table-> index ('usergroup_id');
            $table-> foreign ('usergroup_id')-> references ('id')-> on ('usergroups');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('users', function($table) {
            $table-> dropforeign(['usergroup_id']);
            $table-> dropindex (['usergroup_id']);
           
        });
        
    }
}
