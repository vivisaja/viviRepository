@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Kuisioner</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Kuisioner</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
    <div class="row">
      <div class="col-md-12">
          <div class="box box-primary" >
            <div class="box-header">
              <h3 class="box-title">Isi Kuisioner Baru 
                  <a href="{{ route('addKuisioner') }}" class="btn btn-success btn-flat btn-sm"  title="Tambah"><i class="fa fa-plus"></i></a></h3>
            </div>
            @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                      <p>{{ $message }}</p>
                  </div>
            @endif
            <div class="box-body no-padding">
                <table id="dataTracer" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center;">Tahun Ajaran</th>
                  <th style="text-align: center;">Semester</th>
                  <th style="text-align: center;">Nama Dosen</th>
                  <th style="text-align: center;">Mata Kuliah</th>
                  <th style="text-align: center;">Aksi</th>
                </tr>
                </thead>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
    </div><!-- /.row (main row) -->
    </form>            
@endsection


