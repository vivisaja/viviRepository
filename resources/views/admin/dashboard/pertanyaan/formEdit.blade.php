@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Edit Pertanyaan</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Edit Pertanyaan</li>
          </ol>
@stop
@section('content')
          
    <!-- right column -->
    <div>
    <!-- Horizontal Form -->
    <div class="box box-info">
    <!-- /.box-header -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Ada Kesalahan Input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- form start -->
    <form id="" class="form-horizontal" role="form" method="POST" action="{{ route('updatePertanyaan', $pertanyaan->id) }}">
    @csrf
    @method("PUT")
        <div class="box-body">
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">ID<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="" name="id" value="{{$pertanyaan->id}}" disabled="true">
                </div>
            </div>
            <div class="form-group ">
                <label for="" class="col-sm-2 control-label">Kompetensi<span class="required">*</span></label>
                <div class="col-sm-10 has-error">
                <select class="form-control" name="kompetensi_id">
                @foreach ($kompetensi as $kompetensi)
                <option value="{{ $kompetensi->id }}"
                    @if ($kompetensi->id === $pertanyaan->kompetensi_id)
                        selected
                    @endif
                    >
                    {{$kompetensi->kompetensi}}
                </option>
                @endforeach
                </select>
            </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-2 control-label">Isi Pertanyaan<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="pertanyaan" value="{{$pertanyaan->pertanyaan}}" id="pertanyaan" placeholder="Isi Pertanyaan">                 
                </div>
            </div>               
        <!-- /.box-body -->
        <div class="box-footer" style="width: 59%;">
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                <a href="{{ route('pertanyaan') }}" class="btn btn-primary" type="button">Kembali</a>
                <button type="submit" class="btn btn-success">Perbarui</button>
                </div>
            </div>
            </div>
        </div>
        <!-- /.box-footer -->
    </form>
    </div>
    <!-- /.box -->


@endsection

