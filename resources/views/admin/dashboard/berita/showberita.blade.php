@extends('admin.layout.master')
@section('breadcrump')
          <h1>
            Dashboard
            <small>Berita</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
            <li class="active">Berita</li>
          </ol>
@stop
@section('content')
<form class="form-horizontal">
          <div class="row">
            {{--  <div>  --}}
            <div class="col-md-8">
                <div class="box box-primary" >
                  <div class="box-header">
                    <h3 class="box-title">Data Berita
                        <a href="{{{ route('addBerita') }}}" class="btn btn-success btn-flat btn-sm" data-toggle="modal" title="Tambah"><i class="fa fa-plus"></i></a></h3>
                  </div>
                  <div class="box-body no-padding">
                    <table id = "dataBerita" class="table table-bordered table-hover">
                      <thead>
                      <tr>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">Judul</th>
                        <th style="text-align: center;">Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($berita as $item): ?>
                        <tr>
                          <td>{{$item->id}}</td>
                          <td>{{$item->judul}}</td>                        
                          <td><a href="{{{ URL::to('berita/'.$item->id.'/edit') }}}">
                            <span class="label label-warning"><i class="fa fa-edit"> Edit </i></span>
                            </a> </td>
                        <td><a href="{{{ action('BeritaController@destroy',[$item->id]) }}}" title="hapus"   onclick="return confirm('Apakah anda yakin akan menghapus Berita {{{($item->id).' - '.$item->judul }}}?')">
                            <span class="label label-danger"><i class="fa fa-trash"> Delete </i></span>
                            </a>                          
                        </td> 
                        </tr>
                        <?php endforeach  ?> 
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
          </div><!-- /.row (main row) -->
          </form>
            
@endsection


