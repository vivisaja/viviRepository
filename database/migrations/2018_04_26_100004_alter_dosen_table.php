<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosen', function($table) {
            $table-> index ('user_id');
            $table-> foreign ('user_id')-> references ('id')-> on ('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dosen', function($table) {
            $table-> dropforeign (['user_id']);
            $table-> dropindex (['user_id']);
        });
    }
}
