<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Validator;

// models
use App\User;

class RUser {
    const USERGROUP_ID_ADMIN = 1;
    const USERGROUP_ID_DOSEN = 2;
    const USERGROUP_ID_MAHASISWA = 3;

    public static function runSeeder($data){
        foreach ($data as $item) {
            $user = new User();
            $user->usergroup_id = $item['usergroup_id'];
            $user->nama = $item['nama'];
            $user->email = $item['email'];
            $user->username = $item['username'];
            $user->password = Hash::make($item['password']);
            $user->save();


        }
    }

    // public static function saveMahasiswa($nim, $nama, $email, $password, $angkatan, $is_alumni)
    // {
    //     $fields = array(
    //         'nim' => $nim,
    //         'nama' => $nama,
    //         'angkatan' => $angkatan,
    //         'email' => $email,
    //         'password' => $password,
    //         'is_alumni' => $is_alumni,
    //     );
    //     $rules = array(
    //         'nim' => 'required',
    //         'nama' => 'required',
    //         'angkatan' => 'required',
    //         'email' => 'required',
    //         'password' => 'required',
    //         'is_alumni' => 'required',
    //     );
    //     $messages = array(
    //         'nim.required' => 'NIM tidak boleh kosong',
    //         'nama.required' => 'Nama tidak boleh kosong',
    //         'angkatan.required' => 'Angkatan tidak boleh kosong',
    //         'email.required' => 'Email tidak boleh kosong',
    //         'password.required' => 'Password tidak boleh kosong',
    //         'is_alumni.required' => 'Status Mahasiswa tidak boleh kosong',
    //     );
    //     $validator = Validator::make($fields, $rules, $messages); //prosesnya cek
    //     if ($validator->fails()) {
    //         throw new ValidationException($validator);
    //     }
        
    //     $user = new User();
    //     $user->usergroup_id = self::USERGROUP_ID_MAHASISWA;
    //     $user->nama = $nama;
    //     $user->username = $nim;
    //     $user->email = $email;
    //     $user->password = Hash::make($password);
    //     $user->save();


    //     $mhs = new \App\Mahasiswa;
    //     $mhs->nim = $nim;
    //     $mhs->user_id = $user->id;
    //     $mhs->angkatan = $angkatan;
    //     $mhs->is_alumni = $is_alumni;
    //     $mhs->save();
    // }
}